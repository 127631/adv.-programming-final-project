/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Messages;

import java.awt.Color;
import java.awt.Graphics2D;
import jorgebruned.fun4friends.Games.GameEntity;
import jorgebruned.fun4friends.Games.NotRealTimeGameException;

/**
 * This message type is used to send game updates. It consists on a GameEntity
 * array, so the status of the necessary game entities can be sent to the clients.
 * 
 * Some games have a class extending from this one to include more information
 * 
 * @author Jorge Bruned Alamán
 */
public class GameStatus implements Message {
    
    final GameEntity[] entities;

    public GameStatus(int nEntities) {
        entities = new GameEntity[nEntities];
    }

    public GameStatus(GameEntity[] entities) {
        this.entities = entities;
    }

    public GameEntity[] getEntities() {
        return entities;
    }
    
    public void draw (Graphics2D g2, int panelWidth, int panelHeight) {
        g2.setColor(Color.BLACK);
        g2.fillRect(0, 0, panelWidth, panelHeight);
        g2.setColor(Color.WHITE);
        if (entities != null)
            for (GameEntity ge : entities)
                ge.draw(g2, panelWidth, panelHeight);
    }
    
    public GameControl getUpdate (int x, int y) throws NotRealTimeGameException {
        throw new NotRealTimeGameException();
    }
    
}
