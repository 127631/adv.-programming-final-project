/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Messages;

import java.io.Serializable;

/**
 * This is the interface all the message types implement. The communication
 * between the server and the clients is made using this type of objects.
 * It, of course, implements Serializable so it can be sent through sockets.
 * @author Jorge Bruned Alamán
 */
public interface Message extends Serializable {}
