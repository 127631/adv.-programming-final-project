/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Messages;

/**
 * Same as control message, but used for games (used to communicate client and
 * server, takes an action and optionally some parameters).
 * In this case the actions are defined as constants individually in each game
 * @author Jorge Bruned Alamán
 */
public class GameControl extends ControlMessage {

    public GameControl(int action) {
        super(action);
    }

    public GameControl(int action, String[] params) {
        super(action, params);
    }
    
}
