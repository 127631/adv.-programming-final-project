/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Messages;

/**
 * Message used to communicate the client and the server, takes both an action
 * (integer to optimize the size of the messages, the actions are defined as
 * constants) and, optionally, an array of parameters (String[])
 * @author jbala
 */
public class ControlMessage implements Message {
    
    public static final int ERROR = 0;
    public static final int WELCOME = 1;
    public static final int SET_NAME = 2;
    public static final int LOGIN = 3;
    public static final int REGISTER = 4;
    public static final int AUTH_RESULT = 5;
    public static final int PICK_GAME = 6;
    public static final int GAME_PICKED = 7;
    public static final int START_GAME = 8;
    public static final int GAME_STARTED = 9;
    public static final int END_GAME = 10;
    public static final int GAME_ENDED = 11;
    public static final int UDP_AUTH = 12;
    public static final int MUST_AUTH_UDP = 13;
    
    private final int action;
    private final String[] params;

    public ControlMessage(int action) {
        this.action = action;
        this.params = null;
    }
    
    public ControlMessage(int action, String[] params) {
        this.action = action;
        this.params = params;
    }

    public int getAction() {
        return action;
    }

    public String[] getParams() {
        return params;
    }
    
}
