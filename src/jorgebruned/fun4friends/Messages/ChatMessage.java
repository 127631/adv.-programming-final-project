/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Messages;

import java.sql.Time;

/**
 * Used to store and send though a socket chat messages
 * @author Jorge Bruned Alamán
 */
public class ChatMessage implements Message {

    private final Time timeSent;
    private String userName;
    private final String message;

    public ChatMessage(String message) {
        this.timeSent = new Time( System.currentTimeMillis() );
        this.message = message;
    }
    
    public ChatMessage(String message, String userName) {
        this.timeSent = new Time( System.currentTimeMillis() );
        this.message = message;
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Time getTimeSent() {
        return timeSent;
    }
    
}
