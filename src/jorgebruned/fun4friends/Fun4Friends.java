/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends;

import jorgebruned.fun4friends.Client.ClientGUI;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import jorgebruned.fun4friends.Server.Server;

/**
 * Main executable class of the application.
 * 
 * It automatically tries to starts a client connected to the default socketTCP.
 * If the default socketTCP isn't available, the user can host or join a room
 * (Server and ClientGUI classes can still be executed independently)
 * 
 * @author Jorge Bruned Alamán
 */
public class Fun4Friends {
    
    public static final boolean UNDER_DEVELOPMENT = false;
    public static final String LOCAL_ADDRESS = "127.0.0.1";
    static final String DEFAULT_SERVER =
            UNDER_DEVELOPMENT ? LOCAL_ADDRESS : "188.127.165.187";
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClientGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClientGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClientGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClientGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
                
        try {
            // Try to connect to the default socketTCP
            new ClientGUI(DEFAULT_SERVER, Server.DEFAULT_PORT);
        } catch (IOException ex) { // UnknownHostException is a subclass of IOException
            switch ( UNDER_DEVELOPMENT ? 0 : JOptionPane.showOptionDialog( null ,
                    "Couldn't connect to the default server!", "Server not found",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
                    new String[]{"Host a room", "Conect to a friend's room","Quit"},
                    JOptionPane.DEFAULT_OPTION) ) {
                case 0:
                    // Start a socketTCP if the user has picked to host a room
                    new Thread(new Server()).start();
                    try {
                        // Get public IP address
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                new URL("http://checkip.amazonaws.com").openStream()));
                        String ip = in.readLine();
                        if (!UNDER_DEVELOPMENT)
                            JOptionPane.showMessageDialog(null, "Tell your friends to connect to " + ip +
                                    "!\nPlease, make sure port " + Server.DEFAULT_PORT + " is opened in your router",
                                    "Server started", JOptionPane.INFORMATION_MESSAGE);
                    } catch (IOException ex1) { // MalformedURLException is a subclass of IOException
                        Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    // Start a client connected to the just started socketTCP
                    try {
                        if (UNDER_DEVELOPMENT)
                            new ClientGUI("localhost", Server.DEFAULT_PORT);
                        else
                            new ClientGUI("localhost", Server.DEFAULT_PORT).setServer(true);
                    } catch (IOException ex1) {
                        Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    break;
                case 1:
                    // Ask for an IP and try to connect to the socketTCP
                    try {
                        new ClientGUI(JOptionPane.showInputDialog("What's your friend's IP address?"), Server.DEFAULT_PORT);
                        break;
                    } catch (IOException ex1) {
                        JOptionPane.showMessageDialog(null, "Couldn't connect to the server",
                                "Error", JOptionPane.ERROR_MESSAGE);
                    }
                case 2:
                    // If the user has chosen to quit the app
                    System.exit(0);
            }
        }
    }
    
}
