/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jorgebruned.fun4friends.Messages.ControlMessage;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.Message;

/**
 * This class opens an UDP sockets and receives messages from the clients through it.
 * The first UDP message sent from the client must be a ControlMessage with action
 * UDP_AUTH and the unique client user name as the only parameter
 * @author Jorge Bruned Alamán
 */
public class UDPServer extends Thread {
    
    public static final int BUFFER_LENGTH = 102400;
    public static final int UDP_TIMEOUT = 5; // in seconds
    
    final DatagramSocket sock;
    final Server server;

    public UDPServer(Server server) throws SocketException {
        this.server = server;
        sock = new DatagramSocket(Server.DEFAULT_PORT);
    }
    
    public void send (Message msg, SocketAddress addr) throws IOException {
        if (addr == null)
            throw new IOException("Invalid address");
        final ByteArrayOutputStream byst = new ByteArrayOutputStream(BUFFER_LENGTH);
        final ObjectOutputStream out = new ObjectOutputStream(byst);
        out.writeObject(msg);
        final byte[] data = byst.toByteArray();
        sock.send( new DatagramPacket(data, data.length, addr) );
    }
    
    public void send (Message msg, InetAddress ip, int port) throws IOException {
        synchronized (sock) {
            final ByteArrayOutputStream byst = new ByteArrayOutputStream(BUFFER_LENGTH);
            final ObjectOutputStream out = new ObjectOutputStream(byst);
            out.writeObject(msg);
            final byte[] data = byst.toByteArray();
            sock.send( new DatagramPacket(data, UDPServer.BUFFER_LENGTH, ip, port) );
        }
    }
    
    public DatagramPacket receive() throws IOException {
        synchronized (sock) {
            DatagramPacket received = new DatagramPacket(new byte[BUFFER_LENGTH], BUFFER_LENGTH);
            sock.receive(received);
            return received;
        }
    }
    
    static Message decodeMessage (byte[] data) {
        try {
            Object msg = new ObjectInputStream(
                    new ByteArrayInputStream(data)).readObject();
            if (msg instanceof Message)
                return (Message)msg;
            else
                throw new ClassNotFoundException("Message received through UDP "
                        + "does not implement Message"); // caught right below
        } catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public void run() {
        while ( !interrupted() ) {
            try {
                synchronized ( sock ) {
                    DatagramPacket received = receive();
                    Message msg = decodeMessage(received.getData());
                    ClientHandler ch = server.getClientHandler(received.getSocketAddress());
                    if ( ch == null ) {
                        if ( !(msg instanceof ControlMessage) )
                            continue; // discard the message
                        ControlMessage ctrlMsg = (ControlMessage)msg;
                        if ( ctrlMsg.getAction() != ControlMessage.UDP_AUTH ||
                                (ch = server.getClientHandler( ctrlMsg.getParams()[0] )) == null )
                            continue; // discard the message
                        ch.setUDPaddress( received.getSocketAddress() );
                    }
                    if ( msg instanceof GameControl )
                        ch.gameMessageReceived( (GameControl)msg );
                }
            } catch (IOException ex) {
                Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    
}
