/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Server;

import java.io.IOException;
import java.net.SocketException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jorgebruned.fun4friends.Games.BrickBreaker.BrickBreaker;
import jorgebruned.fun4friends.Games.Game;
import jorgebruned.fun4friends.Games.NotRealTimeGameException;
import jorgebruned.fun4friends.Games.Pictionary.Pictionary;
import jorgebruned.fun4friends.Games.Pong.Pong;
import jorgebruned.fun4friends.Messages.*;

/**
 * This class takes a Game and executes it. This class is responsible for
 * dealing with all the network stuff, which is transparent to the Game objects
 * @author Jorge Bruned Alamán
 */
public class ServerGameExecutor extends Thread {
    
    final Server server;
    final List<ClientHandler> players;
    final Game game;
    
    public ServerGameExecutor (Server server, int gameNum) throws IllegalArgumentException {
        this.server = server;
        switch (gameNum) {
            case Game.PONG:
                game = new Pong( this );
                break;
            case Game.BRICK_BREAKER:
                game = new BrickBreaker( this );
                break;
            case Game.PICTIONARY:
                game = new Pictionary( this );
                break;
            default:
                throw new IllegalArgumentException("Invalid game number");
        }
        players = new LinkedList();
    }
    
    public void addPlayer (ClientHandler player) {
        players.add(player);
    }
    
    public void startGame() {
        sendToAllPlayers( new ControlMessage(ControlMessage.GAME_STARTED) );
        System.out.println("Starting " + game.getClass().getSimpleName() + " game");
        game.init();
        try {
            sendToAllPlayers( game.getStatus(0) );
            Thread.sleep(game.getWaitingTimeBeforeStart() * 1000L);
        } catch (NotRealTimeGameException | InterruptedException ex) {}
        start();
    }

    public void update ( GameStatus status ) {
        sendToAllPlayers(status);
    }
    
    public void logic ( ClientHandler from, GameControl action ) {
        game.logic(players.indexOf(from), action);
    }
    
    public void endGame () {
        sendToAllPlayers( new ControlMessage(ControlMessage.GAME_ENDED) );
        System.out.println("Ending " + game.getClass().getSimpleName() + " game");
        game.end();
        synchronized ( server.ongoingGames ) {
            server.ongoingGames.remove( this );
        }
        this.interrupt();
    }

    @Override
    public void run() {
        long prevMiliseconds = System.currentTimeMillis();
        if (game.getFPS() == 0)
            return;
        final long milisBetweenFrames = 1000 / game.getFPS();
        while ( !this.interrupted() && !game.isOver() ) {
            try {
                long miliseconds = System.currentTimeMillis();
                sendToAllPlayersUDP( game.getStatus( (miliseconds - prevMiliseconds)/(double)1000 ) );
                prevMiliseconds = miliseconds;
                Thread.sleep( milisBetweenFrames );
            } catch (InterruptedException | NotRealTimeGameException ex) {
                break;
            }
        }
    }
    
    public boolean removePlayer (ClientHandler player) {
        synchronized (players) {
            int nPlayer = players.indexOf(player);
            if (nPlayer != -1 && players.remove(player)) {
                game.playerRemoved( nPlayer );
                return true;
            }
            return false;
        }
    }
    
    public int nPlayers () {
        return players.size();
    }
    
    public void sendToAllPlayers(Message msg) {
        Iterator<ClientHandler> iterator;
        synchronized (players) {
            iterator = players.iterator();
        }
        while (iterator.hasNext()) {
            ClientHandler p = iterator.next();
            try {
                p.sendTCP(msg);
                p.resetOutputStream();
            } catch (SocketException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                //System.out.println("Player disconnected during the game");
                removePlayer(p);
            } catch (IOException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void sendToAllPlayersUDP(Message msg) {
        Iterator<ClientHandler> iterator;
        synchronized (players) {
            iterator = players.iterator();
        }
        while (iterator.hasNext()) {
            ClientHandler p = iterator.next();
            try {
                p.sendUDP(msg);
            } catch (SocketException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                //System.out.println("Player disconnected during the game");
                removePlayer(p);
            } catch (IOException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void sendToAllPlayersExcept(Message msg, int nPlayerExclude) {
        Iterator<ClientHandler> iterator;
        synchronized (players) {
            iterator = players.iterator();
        }
        while (iterator.hasNext()) {
            ClientHandler p = iterator.next();
            if (players.indexOf(p) == nPlayerExclude)
                continue;
            try {
                p.sendTCP(msg);
                p.resetOutputStream();
            } catch (SocketException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                //System.out.println("Player disconnected during the game");
                removePlayer(p);
            } catch (IOException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void sendToPlayer(Message msg, int nPlayer) {
        ClientHandler p;
        synchronized (players) {
            p = players.get(nPlayer);
        }
        try {
            p.sendTCP(msg);
            p.resetOutputStream();
        } catch (SocketException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            //System.out.println("Player disconnected during the game");
            removePlayer(p);
        } catch (IOException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getPlayerName(int nPlayer) {
        synchronized (players) {
            return players.get(nPlayer).user.getName();
        }
    }
    
}
