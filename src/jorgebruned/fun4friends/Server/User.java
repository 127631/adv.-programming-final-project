/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Server;

/**
 * This dummy class just stores the name of the current user.
 * I've decided to keep it anyway since it provides a lot of scalability in case
 * more data is stored for the clients in the future, or to store the users on a
 * binary file or database to allow them to register and/or logging instead of
 * just entering their name every time they start the application
 * @author Jorge Bruned Alamán
 */
public class User {
    
    String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
