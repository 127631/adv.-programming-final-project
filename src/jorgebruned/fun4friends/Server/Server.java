/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Server;

import jorgebruned.fun4friends.Messages.Message;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import jorgebruned.fun4friends.Games.Game;

/**
 * This class implements the server. It constantly listens for new incoming
 * connections and starts a new ClientHandler every time. Contains the list
 * of online users and ongoing games.
 * This class can be run independently (it contains a public static void main
 * or it can be run from the main executable of the project (Fun4Friends)
 * @author Jorge Bruned Alamán
 */
public class Server implements Runnable {
    
    public static final int DEFAULT_PORT = 5678;
    
    public final Map<Integer,Queue<ClientHandler>> waitingPlayers = new HashMap();
    public final List<ServerGameExecutor> ongoingGames = new LinkedList();
    private UDPServer udpServer;
    
    public Server() {
        for (int i=Game.NONE; i<=Game.PACHISI; i++)
            waitingPlayers.put(i, new LinkedList());
        try {
            udpServer = new UDPServer(this);
            udpServer.start();
        } catch (SocketException ex) {
            udpServer = null;
        }
    }
    
    public boolean userNameTaken(String name) {
        synchronized (waitingPlayers) {
            for (Queue<ClientHandler> q : waitingPlayers.values())
                if (q.stream().anyMatch( u -> u.user != null && u.user.getName().equalsIgnoreCase(name) ))
                    return true;
        }
        return false;
    }
    
    public void sendToAllClients (Message msg) {
        synchronized (waitingPlayers) {
            for (Queue<ClientHandler> q : waitingPlayers.values())
                q.stream().forEach( ch -> {
                    try {
                        ch.sendTCP( msg );
                    } catch (IOException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } );
        }
    }
    
    public boolean sendUDP (Message msg, SocketAddress addr) throws IOException {
        if (udpServer == null)
            return false;
        udpServer.send(msg, addr);
        return true;
    }
    
    public ClientHandler getClientHandler (SocketAddress address) {
        synchronized (waitingPlayers) {
            for (Queue<ClientHandler> q : waitingPlayers.values())
                for (ClientHandler ch : q)
                    if (address.equals(ch.getUDPaddress()))
                        return ch;
        }
        return null;
    }
    
    public ClientHandler getClientHandler (String userName) {
        synchronized (waitingPlayers) {
            for (Queue<ClientHandler> q : waitingPlayers.values())
                for (ClientHandler ch : q)
                    if (userName.equals(ch.user.getName()))
                        return ch;
        }
        return null;
    }
    
    public void run() {
        final ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(DEFAULT_PORT);
            System.out.println("Started server on port " + DEFAULT_PORT);

            while (true) {
                ClientHandler ch = new ClientHandler( this, serverSocket.accept() );
                System.out.println( "Accepted connection from "
                        + ch.socket.getInetAddress() + ":" + ch.socket.getPort() );
                ch.start();
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        
        new Server().run();
        
    }
}
