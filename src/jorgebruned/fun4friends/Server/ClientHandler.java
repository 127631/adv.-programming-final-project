/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import jorgebruned.fun4friends.Games.*;
import jorgebruned.fun4friends.Messages.*;

/**
 * This class is instantiated every time a client connects to the server and
 * handles the client's requests as well as constantly listening for incoming
 * requests. When a message is received, either the action requested is done
 * or the message is forwarded to another class (for example, game updates
 * are forwaded to the GameExecutor).
 * It, of course, extends from Thread so we can handle multiple clients at once
 * @author Jorge Bruned Alamán
 */
public class ClientHandler extends Thread {
        
        final Server server;
        final Socket socket;
        final ObjectOutputStream out;
        final ObjectInputStream in;
        public User user;
        ServerGameExecutor ge;
        SocketAddress udpAddress = null;
        long lastUDPcommunication;

        public ClientHandler (Server server, Socket socket) throws IOException {
            this.server = server;
            this.socket = socket;
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        }
        
        public void sendTCP (Message msg) throws IOException {
            synchronized (out) {
                try {
                    out.writeObject( msg );
                } catch (SocketException ex) {
                    this.interrupt();
                }
            }
        }
        
        public void resetOutputStream() {
            synchronized (out) {
                try {
                    out.reset();
                } catch (IOException ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public Message receive() throws IOException, SocketException {
            synchronized (in) {
                try {
                    Object msg;
                    if ((msg = in.readObject()) instanceof Message)
                        return (Message)msg;
                    else
                        return null;
                } catch (ClassNotFoundException e) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, e);
                    return null;
                }
            }
        }
        
        public void sendAllPickedGames() {
            synchronized (server.waitingPlayers) {
                for (final int i : server.waitingPlayers.keySet())
                    server.waitingPlayers.get(i).forEach(ch -> {
                        try {
                            if (ch.user != null)
                                this.sendTCP( new ControlMessage(ControlMessage.GAME_PICKED,
                                        new String[]{ch.user.getName(),i+""}) );
                        } catch (IOException ex) {
                            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } );
            }
        }
        
        public boolean cancelPickedGame() {
            synchronized (server.waitingPlayers) {
                for (Queue<ClientHandler> q : server.waitingPlayers.values())
                    if (q.remove(this))
                        return true;
            }
            return false;
        }
        
        public boolean quitOngoingGame () {
            synchronized (server.ongoingGames) {
                for (ServerGameExecutor g : server.ongoingGames)
                    if (g.removePlayer( this ))
                        return true;
            }
            return false;
        }
        
        public int getGameNumber() {
            synchronized (server.waitingPlayers) {
                for (int i : server.waitingPlayers.keySet())
                    if (server.waitingPlayers.get(i).contains(this))
                        return i;
            }
            return Game.NONE;
        }
        
        public void gameMessageReceived (GameControl msg) {
            ge.logic( this, msg );
            lastUDPcommunication = System.currentTimeMillis();
        }
        
        public SocketAddress getUDPaddress() {
            long currentMillis = System.currentTimeMillis();
            if (currentMillis > lastUDPcommunication + UDPServer.UDP_TIMEOUT * 1000)
                return (udpAddress = null);
            lastUDPcommunication = currentMillis;
            return udpAddress;
        }
        
        public void setUDPaddress(SocketAddress addr) {
            udpAddress = addr;
            lastUDPcommunication = System.currentTimeMillis();
        }
        
        public void sendUDP (Message msg) throws IOException {
            long currentMillis = System.currentTimeMillis();
            if (currentMillis > lastUDPcommunication + UDPServer.UDP_TIMEOUT * 1000) {
                udpAddress = null;
                sendTCP( new ControlMessage(ControlMessage.MUST_AUTH_UDP) );
                sendTCP(msg);
                //throw new IOException("UDP Timeout has been exceeded");
            }
            lastUDPcommunication = currentMillis;
            try {
                if (!server.sendUDP(msg, udpAddress))
                    throw new IOException("UDP connection not available");
            } catch (IOException ex) {
                sendTCP(msg);
            }
        }
        
        @Override
        public void run () {
            try {
                Message msg;
                // 1. Authentication
                sendTCP( new ControlMessage(ControlMessage.WELCOME, null) );
                while (true) {
                    msg = receive();
                    if ( ((ControlMessage)msg).getAction() != ControlMessage.SET_NAME ) {
                        System.out.println("Client disconnected from "
                                + socket.getInetAddress() + ": didn't send auth request");
                        socket.close();
                        return;
                    }
                    if ( !server.userNameTaken(((ControlMessage)msg).getParams()[0]) )
                        break;
                    sendTCP( new ControlMessage(ControlMessage.AUTH_RESULT,
                            new String[]{"0","That username is already taken"}) );
                }
                System.out.println("Client from " + socket.getInetAddress() +
                        ": auth successful with username " + ((ControlMessage)msg).getParams()[0]);
                sendTCP( new ControlMessage(ControlMessage.AUTH_RESULT, new String[]{"1"}) );
                this.user = new User( ((ControlMessage)msg).getParams()[0] );
                sendAllPickedGames();
                synchronized ( server.waitingPlayers ) {
                    server.waitingPlayers.get( Game.NONE ).add( this );
                }
                // 2. Attend user requests
                while ( (msg = receive()) != null ) {
                    if (msg instanceof ChatMessage) {
                        ((ChatMessage)msg).setUserName(user.name);
                        // filter bad words ... ?
                        server.sendToAllClients( msg );
                    } else if ((msg instanceof GameControl) && ge != null) {
                        gameMessageReceived((GameControl)msg);
                    } else if (msg instanceof ControlMessage) {
                        switch ( ((ControlMessage)msg).getAction() ) {
                            case ControlMessage.PICK_GAME:
                                final int nGame = 
                                        Integer.parseInt( ((ControlMessage)msg).getParams()[0] );
                                synchronized (server.waitingPlayers) {
                                    server.waitingPlayers.values().forEach( q -> q.remove(this) );
                                    server.waitingPlayers.get(nGame).add(this);
                                }
                                server.sendToAllClients( new ControlMessage(ControlMessage.GAME_PICKED,
                                        new String[]{user.name, nGame+""}) );
                                break;
                                
                            case ControlMessage.START_GAME:
                                final int gameNum = getGameNumber();
                                try {
                                    ge = new ServerGameExecutor( server, gameNum );
                                } catch (IllegalArgumentException e) {
                                    //System.out.println("Invalid game");
                                    sendTCP( new ControlMessage(ControlMessage.ERROR,
                                            new String[]{"That game is not available yet"}) );
                                    break;
                                }
                                
                                synchronized (server.waitingPlayers) {
                                    final Queue<ClientHandler> none = server.waitingPlayers.get(Game.NONE);
                                    final Queue<ClientHandler> q = server.waitingPlayers.get(gameNum);
                                    if (ge.game.minPlayers() > q.size()) {
                                        this.sendTCP( new ControlMessage(ControlMessage.ERROR,
                                                new String[]{"Not enough players"}) );
                                        System.out.println("Not enough players");
                                        break;
                                    }
                                    ge.addPlayer( this );
                                    q.remove( this );
                                    none.add(this);
                                    for (int i=1; i<ge.game.maxPlayers(); i++) {
                                        final ClientHandler player = q.poll();
                                        if (player == null)
                                            break;
                                        server.sendToAllClients( new ControlMessage(ControlMessage.GAME_PICKED,
                                            new String[]{player.user.name, Game.NONE+""}) );
                                        ge.addPlayer(player);
                                        player.ge = ge;
                                        none.add(player);
                                    }
                                }
                                synchronized (server.ongoingGames) {
                                    server.ongoingGames.add(ge);
                                }
                                ge.startGame();
                                System.out.println("Client from " + socket.getInetAddress()
                                        + " has started the game");
                                break;
                                
                            case ControlMessage.END_GAME:
                                System.out.println("Client from " + socket.getInetAddress()
                                        + " has left the game");
                                ge.removePlayer(this);
                                break;
                        }
                    }
                }
            } catch (SocketException e) {
                System.out.println("Client from " + socket.getInetAddress() +
                        " has disconnected");
            } catch (IOException e) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, e);
            } finally {
                if (this.ge != null)
                    synchronized (server.ongoingGames) {
                        ge.removePlayer( this );
                    }
                synchronized (server.waitingPlayers) {
                    server.waitingPlayers.values().forEach( q -> q.remove( this ) );
                }
                server.sendToAllClients( new ControlMessage(ControlMessage.GAME_PICKED,
                        new String[]{user.name, Game.NONE+""}) );
                try {
                    out.close();
                    in.close();
                    socket.close();
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
