/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import jorgebruned.fun4friends.Games.NotRealTimeGameException;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.GameStatus;

/**
 * This abstract class is extended by the GUIs of every game.
 * It includes common things to all GUIs and specific GUIs are implemented
 * for the different games.
 * @author Jorge Bruned Alamán
 */
public abstract class GameGUI extends JFrame {
    
    ClientGameExecutor gameExec;

    public GameGUI(ClientGameExecutor gameExec) {
        this.gameExec = gameExec;
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                if (JOptionPane.showConfirmDialog(null, "Are you sure you want to exit the game?",
                        "Confirm",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE)
                        == JOptionPane.YES_OPTION) {
                    gameExec.endGame();
                }
            }
        });
    }
    
    public void relocateChat(JFrame chatFrame) {
        chatFrame.setLocationRelativeTo(this);
        chatFrame.setSize(chatFrame.getSize().width, getSize().height);
        chatFrame.setLocation(getLocationOnScreen().x + getSize().width,
                this.getLocationOnScreen().y);
    }
    
    public abstract GameControl getInput() throws NotRealTimeGameException;
    public abstract void updateGame(GameStatus status);
    public abstract void updateGame(GameControl control);
    
}
