/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Client;

import jorgebruned.fun4friends.Messages.GameStatus;
import jorgebruned.fun4friends.Messages.GameControl;

/**
 * This class is responsible for executing the game on the client side. Although
 * it's only extended by ClientGameExecutor, I kept the abstract class since it
 * provides a lot of scalability, allowing, for example, to create a subclass
 * LocalGameExecutor, to play games offline or even with bots.
 * @author Jorge Bruned Alamán
 */
public abstract class GameExecutor extends Thread {
    
    public abstract void logic ( GameControl action );
    public abstract void update ( GameStatus status );
    public abstract void update ( GameControl control );
    
}
