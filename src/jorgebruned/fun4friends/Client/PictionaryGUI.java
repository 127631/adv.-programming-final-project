/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Client;

import jorgebruned.fun4friends.Games.NotRealTimeGameException;
import jorgebruned.fun4friends.Games.Pictionary.Pictionary;
import jorgebruned.fun4friends.Games.Pictionary.Drawing;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.GameStatus;

/**
 * This class implements the GUI to play Pictionary, which is different from
 * the generic one since it needs quite a lot of buttons and inputs
 * @author Jorge Bruned Alamán
 */
public class PictionaryGUI extends GameGUI {
    
    int currentColor = Pictionary.BLACK;
    boolean nowDrawing = false;

    public PictionaryGUI (ClientGameExecutor gameExec) {
        super(gameExec);
        initComponents();
        setSize((int)(1.2*Drawing.WIDTH), (int)(1.2*Drawing.WIDTH/Pictionary.ASPECT_RATIO));
        setLocationRelativeTo( null );
    }

    @Override
    public GameControl getInput() throws NotRealTimeGameException {
        throw new NotRealTimeGameException();
    }
    
    @Override
    public void updateGame (GameStatus status) {
        if (status != null)
            ((DrawPanel)drawPanel).update( status );
    }
    
    public void setDrawingMode (boolean drawingMode, String wordOrName) {
        toDoText.setText(drawingMode ? "You have to draw:" : wordOrName + " is drawing");
        if (drawingMode)
            toDrawText.setText(wordOrName);
        
        blackButton.setVisible(drawingMode);
        redButton.setVisible(drawingMode);
        blueButton.setVisible(drawingMode);
        yellowButton.setVisible(drawingMode);
        greenButton.setVisible(drawingMode);
        rubberButton.setVisible(drawingMode);
        orangeButton.setVisible(drawingMode);
        pinkButton.setVisible(drawingMode);
        toDrawText.setVisible(drawingMode);
        sizeSlider.setVisible(drawingMode);
        
        guessInput.setVisible(!drawingMode);
        guessButton.setVisible(!drawingMode);
        drawPanel.setCursor(drawingMode ? new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR)
                : new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        
        nowDrawing = drawingMode;
    }
    
    @Override
    public void updateGame (GameControl control) {
        setDrawingMode(control.getAction() == Pictionary.TURN_DRAW,control.getParams()[0]);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        drawPanel = new DrawPanel();
        toDoText = new javax.swing.JLabel();
        toDrawText = new javax.swing.JLabel();
        guessInput = new javax.swing.JTextField();
        guessButton = new javax.swing.JButton();
        sizeSlider = new javax.swing.JSlider();
        blackButton = new javax.swing.JToggleButton();
        redButton = new javax.swing.JToggleButton();
        greenButton = new javax.swing.JToggleButton();
        blueButton = new javax.swing.JToggleButton();
        yellowButton = new javax.swing.JToggleButton();
        pinkButton = new javax.swing.JToggleButton();
        orangeButton = new javax.swing.JToggleButton();
        rubberButton = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        drawPanel.setBackground(new java.awt.Color(0, 0, 0));
        drawPanel.setForeground(new java.awt.Color(255, 255, 255));
        drawPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
        drawPanel.setFocusable(false);
        drawPanel.setOpaque(false);
        drawPanel.setRequestFocusEnabled(false);
        drawPanel.setVerifyInputWhenFocusTarget(false);
        drawPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                drawPanelMouseDragged(evt);
            }
        });
        drawPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                drawPanelMouseClicked(evt);
            }
        });

        toDoText.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        toDoText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        toDoText.setText("You have to draw:");
        toDoText.setFocusable(false);

        toDrawText.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        toDrawText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        toDrawText.setText("Something");
        toDrawText.setFocusable(false);

        guessInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guessInputActionPerformed(evt);
            }
        });

        guessButton.setText("Make your guess!");
        guessButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        guessButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guessButtonActionPerformed(evt);
            }
        });

        sizeSlider.setMinimum(2);
        sizeSlider.setOrientation(javax.swing.JSlider.VERTICAL);
        sizeSlider.setPaintTicks(true);
        sizeSlider.setValue(10);
        sizeSlider.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        blackButton.setBackground(java.awt.Color.black);
        blackButton.setSelected(true);
        blackButton.setToolTipText("Set color to Black");
        blackButton.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 20, true));
        blackButton.setContentAreaFilled(false);
        blackButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        blackButton.setPreferredSize(new java.awt.Dimension(30, 30));
        blackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blackButtonActionPerformed(evt);
            }
        });

        redButton.setBackground(java.awt.Color.red);
        redButton.setToolTipText("Set color to Red");
        redButton.setBorder(new javax.swing.border.LineBorder(java.awt.Color.red, 20, true));
        redButton.setContentAreaFilled(false);
        redButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        redButton.setPreferredSize(new java.awt.Dimension(30, 30));
        redButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redButtonActionPerformed(evt);
            }
        });

        greenButton.setBackground(java.awt.Color.green);
        greenButton.setToolTipText("Set color to Green");
        greenButton.setBorder(new javax.swing.border.LineBorder(java.awt.Color.green, 20, true));
        greenButton.setContentAreaFilled(false);
        greenButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        greenButton.setPreferredSize(new java.awt.Dimension(30, 30));
        greenButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                greenButtonActionPerformed(evt);
            }
        });

        blueButton.setBackground(java.awt.Color.blue);
        blueButton.setToolTipText("Set color to Blue");
        blueButton.setBorder(new javax.swing.border.LineBorder(java.awt.Color.blue, 20, true));
        blueButton.setContentAreaFilled(false);
        blueButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        blueButton.setPreferredSize(new java.awt.Dimension(30, 30));
        blueButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blueButtonActionPerformed(evt);
            }
        });

        yellowButton.setBackground(java.awt.Color.yellow);
        yellowButton.setToolTipText("Set color to Yellow");
        yellowButton.setBorder(new javax.swing.border.LineBorder(java.awt.Color.yellow, 20, true));
        yellowButton.setContentAreaFilled(false);
        yellowButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        yellowButton.setPreferredSize(new java.awt.Dimension(30, 30));
        yellowButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yellowButtonActionPerformed(evt);
            }
        });

        pinkButton.setBackground(java.awt.Color.magenta);
        pinkButton.setToolTipText("Set color to Pink");
        pinkButton.setBorder(new javax.swing.border.LineBorder(java.awt.Color.magenta, 20, true));
        pinkButton.setContentAreaFilled(false);
        pinkButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pinkButton.setPreferredSize(new java.awt.Dimension(30, 30));
        pinkButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pinkButtonActionPerformed(evt);
            }
        });

        orangeButton.setBackground(java.awt.Color.orange);
        orangeButton.setToolTipText("Set color to Orange");
        orangeButton.setBorder(new javax.swing.border.LineBorder(java.awt.Color.orange, 20, true));
        orangeButton.setContentAreaFilled(false);
        orangeButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        orangeButton.setPreferredSize(new java.awt.Dimension(30, 30));
        orangeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                orangeButtonActionPerformed(evt);
            }
        });

        rubberButton.setBackground(java.awt.Color.white);
        rubberButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jorgebruned/fun4friends/Games/Pictionary/rubber.png"))); // NOI18N
        rubberButton.setToolTipText("Use rubber");
        rubberButton.setBorder(null);
        rubberButton.setBorderPainted(false);
        rubberButton.setContentAreaFilled(false);
        rubberButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rubberButton.setFocusPainted(false);
        rubberButton.setPreferredSize(new java.awt.Dimension(30, 30));
        rubberButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rubberButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout drawPanelLayout = new javax.swing.GroupLayout(drawPanel);
        drawPanel.setLayout(drawPanelLayout);
        drawPanelLayout.setHorizontalGroup(
            drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(drawPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(toDoText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(toDrawText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, drawPanelLayout.createSequentialGroup()
                        .addGap(0, 150, Short.MAX_VALUE)
                        .addComponent(guessInput, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(guessButton)
                        .addGap(0, 150, Short.MAX_VALUE))
                    .addGroup(drawPanelLayout.createSequentialGroup()
                        .addComponent(sizeSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(blackButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(redButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(greenButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(blueButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(yellowButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pinkButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(orangeButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rubberButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        drawPanelLayout.setVerticalGroup(
            drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(drawPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(toDoText)
                .addGap(0, 0, 0)
                .addComponent(toDrawText)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addGroup(drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(drawPanelLayout.createSequentialGroup()
                        .addComponent(blackButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(redButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(greenButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(blueButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(yellowButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pinkButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(orangeButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rubberButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(sizeSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addGroup(drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guessInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(guessButton))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drawPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drawPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void blackButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blackButtonActionPerformed
        currentColor = Pictionary.BLACK;
    }//GEN-LAST:event_blackButtonActionPerformed

    private void redButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redButtonActionPerformed
        currentColor = Pictionary.RED;
    }//GEN-LAST:event_redButtonActionPerformed

    private void greenButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_greenButtonActionPerformed
        currentColor = Pictionary.GREEN;
    }//GEN-LAST:event_greenButtonActionPerformed

    private void blueButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blueButtonActionPerformed
        currentColor = Pictionary.BLUE;
    }//GEN-LAST:event_blueButtonActionPerformed

    private void yellowButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yellowButtonActionPerformed
        currentColor = Pictionary.YELLOW;
    }//GEN-LAST:event_yellowButtonActionPerformed

    private void pinkButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pinkButtonActionPerformed
        currentColor = Pictionary.PINK;
    }//GEN-LAST:event_pinkButtonActionPerformed

    private void orangeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_orangeButtonActionPerformed
        currentColor = Pictionary.ORANGE;
    }//GEN-LAST:event_orangeButtonActionPerformed

    private void rubberButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rubberButtonActionPerformed
        currentColor = Pictionary.WHITE;
    }//GEN-LAST:event_rubberButtonActionPerformed

    private void draw (int x, int y) {
        gameExec.logic( new GameControl(Pictionary.DRAW, new String[]{
            currentColor+"", sizeSlider.getValue()+"",
            (int)(x/(double)drawPanel.getWidth()*Drawing.WIDTH)+"",
            (int)(y/(double)drawPanel.getHeight()*Drawing.WIDTH/Pictionary.ASPECT_RATIO)+""
        }) );
    }
    
    private void drawPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawPanelMouseClicked
        draw(evt.getX(), evt.getY());
    }//GEN-LAST:event_drawPanelMouseClicked

    private void drawPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawPanelMouseDragged
        draw(evt.getX(), evt.getY());
    }//GEN-LAST:event_drawPanelMouseDragged

    private void guessInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guessInputActionPerformed
        guessButton.doClick();
    }//GEN-LAST:event_guessInputActionPerformed

    private void guessButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guessButtonActionPerformed
        if (guessInput.getText().isEmpty())
            return;
        gameExec.logic( new GameControl(Pictionary.GUESS, new String[]{
                guessInput.getText()}) );
        guessInput.setText( null );
    }//GEN-LAST:event_guessButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton blackButton;
    private javax.swing.JToggleButton blueButton;
    private javax.swing.JPanel drawPanel;
    private javax.swing.JToggleButton greenButton;
    private javax.swing.JButton guessButton;
    private javax.swing.JTextField guessInput;
    private javax.swing.JToggleButton orangeButton;
    private javax.swing.JToggleButton pinkButton;
    private javax.swing.JToggleButton redButton;
    private javax.swing.JToggleButton rubberButton;
    private javax.swing.JSlider sizeSlider;
    private javax.swing.JLabel toDoText;
    private javax.swing.JLabel toDrawText;
    private javax.swing.JToggleButton yellowButton;
    // End of variables declaration//GEN-END:variables

}
