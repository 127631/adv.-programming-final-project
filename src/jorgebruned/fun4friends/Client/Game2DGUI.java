/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Client;

import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import jorgebruned.fun4friends.Games.Game;
import jorgebruned.fun4friends.Games.NotRealTimeGameException;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.GameStatus;

/**
 * Implements the GUI for playing Pong, extending the abstract class GameGUI.
 * Some of the specific Pong code is here but all the GUI stuff is on GameGUI
 * @author Jorge Bruned Alamán
 */
public class Game2DGUI extends GameGUI {
    
    static final int DEFAULT_HEIGHT = Math.min((int)Toolkit.getDefaultToolkit()
            .getScreenSize().getHeight(), 800);
    int xDir = 0, yDir = 0;

    public Game2DGUI (ClientGameExecutor gameExec, double aspectRatio) {
        super(gameExec);
        initComponents();
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_RIGHT:
                        xDir = 1;
                        break;
                    case KeyEvent.VK_LEFT:
                        xDir = -1;
                        break;
                    case KeyEvent.VK_DOWN:
                        yDir = 1;
                        break;
                    case KeyEvent.VK_UP:
                        yDir = -1;
                        break;
                    default:
                        break;
                }
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if ( (e.getKeyCode() == KeyEvent.VK_RIGHT && xDir == 1) ||
                        (e.getKeyCode() == KeyEvent.VK_LEFT && xDir == -1) )
                    xDir = 0;
                else if ( (e.getKeyCode() == KeyEvent.VK_DOWN && yDir == 1) ||
                        (e.getKeyCode() == KeyEvent.VK_UP && yDir == -1) )
                    yDir = 0;
            }
        });
        this.setSize((int)(DEFAULT_HEIGHT * aspectRatio), DEFAULT_HEIGHT);
        this.setLocationRelativeTo( null );
        startCountdown();
    }
    
    private void startCountdown() {
        new Thread( new Runnable() {
            @Override
            public void run() {
                JOptionPane message = new JOptionPane("Game will start in 0 seconds",
                        JOptionPane.INFORMATION_MESSAGE);
                message.setOptions(new Object[]{});
                JDialog dialog = message.createDialog("Game starting");
                Thread countdown = new Thread( new Runnable() {
                    @Override
                    public void run() {
                        for (int i=Game.TIME_BEFORE_START; i>0; i--) {
                            message.setMessage("Game will start in " + i + " seconds");
                            dialog.repaint();
                            try {
                                Thread.sleep(1000L);
                            } catch (InterruptedException ex) {}
                        }
                        dialog.dispose();
                    }
                });
                countdown.start();
                dialog.setVisible(true);
            }
        }).start();
    }

    @Override
    public GameControl getInput() throws NotRealTimeGameException {
        return ((DrawPanel)drawPanel).status == null ? null : 
                ((DrawPanel)drawPanel).status.getUpdate(xDir, yDir);
    }
    
    @Override
    public void updateGame (GameStatus status) {
        ((DrawPanel)drawPanel).update( status );
    }
    
    @Override
    public void updateGame (GameControl control) {}
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        drawPanel = new DrawPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        drawPanel.setBackground(new java.awt.Color(0, 0, 0));
        drawPanel.setForeground(new java.awt.Color(255, 255, 255));
        drawPanel.setFocusable(false);
        drawPanel.setOpaque(false);
        drawPanel.setRequestFocusEnabled(false);
        drawPanel.setVerifyInputWhenFocusTarget(false);

        javax.swing.GroupLayout drawPanelLayout = new javax.swing.GroupLayout(drawPanel);
        drawPanel.setLayout(drawPanelLayout);
        drawPanelLayout.setHorizontalGroup(
            drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 459, Short.MAX_VALUE)
        );
        drawPanelLayout.setVerticalGroup(
            drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drawPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drawPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel drawPanel;
    // End of variables declaration//GEN-END:variables

}
