/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Client;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jorgebruned.fun4friends.Games.BrickBreaker.BrickBreaker;
import jorgebruned.fun4friends.Games.Game;
import jorgebruned.fun4friends.Games.NotRealTimeGameException;
import jorgebruned.fun4friends.Games.Pong.Pong;
import jorgebruned.fun4friends.Messages.ControlMessage;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.GameStatus;

/**
 * This class is responsible for running games on the client side.
 * @author Jorge Bruned Alamán
 */
public class ClientGameExecutor extends GameExecutor {
    
    ClientGUI mainFrame;
    GameGUI gameFrame;
    boolean gameOver = false;
    
    public ClientGameExecutor (ClientGUI mainFrame, int gameNum) throws IllegalArgumentException {
        this.mainFrame = mainFrame;
        switch (gameNum) {
            case Game.PONG:
                gameFrame = new Game2DGUI( this, Pong.ASPECT_RATIO );
                break;
            case Game.BRICK_BREAKER:
                gameFrame = new Game2DGUI( this, BrickBreaker.ASPECT_RATIO );
                break;
            case Game.PICTIONARY:
                gameFrame = new PictionaryGUI( this );
                break;
            default:
                throw new IllegalArgumentException("Invalid game number");
        }
        gameFrame.setVisible( true );
        gameFrame.relocateChat(mainFrame.chatFrame);
    }

    @Override
    public void update ( GameStatus status ) {
        gameFrame.updateGame(status);
    }
    
    @Override
    public void update ( GameControl control ) {
        gameFrame.updateGame(control);
    }
    
    @Override
    public void logic ( GameControl action ) {
        try {
            mainFrame.sendTCP( action );
        } catch (IOException ex) {
            Logger.getLogger(ClientGameExecutor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void sendUDP ( GameControl action ) {
        try {
            mainFrame.sendUDP( action );
        } catch (IOException ex) {
            try {
                mainFrame.sendTCP( action );
            } catch (IOException ex1) {
                Logger.getLogger(ClientGameExecutor.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void endGame () {
        try {
            mainFrame.sendTCP( new ControlMessage(ControlMessage.END_GAME) );
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            gameEnded();    
        }
    }
    
    public void gameEnded () {
        gameFrame.dispose();
        mainFrame.display();
        gameOver = true;
        this.interrupt();
    }

    @Override
    public void run() {
        final int millisBetweenFrames = 1000 / Game.DEFAULT_FPS;
        while ( !this.interrupted() && !gameOver ) {
            try {
                GameControl input = gameFrame.getInput();
                if (input != null) sendUDP( input );
                Thread.sleep( millisBetweenFrames );
            } catch (InterruptedException | NotRealTimeGameException e) {
                break;
            }
        }
    }
    
}
