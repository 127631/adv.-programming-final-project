/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Client;

import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import jorgebruned.fun4friends.Messages.GameStatus;

/**
 * This class implements a draw panel where we can draw with Graphics2D
 * @author Jorge Bruned Alamán
 */
public class DrawPanel extends JPanel {
    
    public GameStatus status;

    public void update (GameStatus status) {
        this.status = status;
        repaint();
    }

    @Override
    public void paintComponent (Graphics g) {
        if (status != null)
            status.draw( (Graphics2D)g, getWidth(), getHeight() );
    }
    
}
