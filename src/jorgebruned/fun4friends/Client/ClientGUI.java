/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Client;

import java.awt.Frame;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import jorgebruned.fun4friends.Fun4Friends;
import jorgebruned.fun4friends.Games.*;
import jorgebruned.fun4friends.Messages.*;
import jorgebruned.fun4friends.Server.Server;
import jorgebruned.fun4friends.Server.UDPServer;
import static jorgebruned.fun4friends.Server.UDPServer.BUFFER_LENGTH;

/**
 * Main client class. It implements the Graphic User Interface and sends user
 requests to the socketTCP. It contains the subclass ServerListener, which is
 constantly reading incoming messages from the socketTCP though the socket.
 This class can be run independently (it contains a public static void main
 or it can be run from the main executable of the project (Fun4Friends)
 * @author Jorge Bruned Alamán
 */
public class ClientGUI extends javax.swing.JFrame {
    
    static final int CONNECTION_TIMEOUT = 3; // in seconds
    
    final Socket socketTCP;
    final ObjectOutputStream out;
    final ObjectInputStream in;
    
    final UDPListener udp;
    boolean isUDPavailable = true;
    
    final String userName;
    final Map<Integer,List<String>> waitingPlayers = new HashMap();
    final ChatGUI chatFrame;
    int selectedGame = Game.NONE;
    ClientGameExecutor gameExec = null;

    public ClientGUI(String ipAddress, int tcpPort) throws IOException, UnknownHostException {
        
        socketTCP = new Socket();   
        socketTCP.connect(new InetSocketAddress(ipAddress,tcpPort), CONNECTION_TIMEOUT * 1000); 
        out = new ObjectOutputStream(socketTCP.getOutputStream());
        in = new ObjectInputStream(socketTCP.getInputStream());
        userName = authenticate();
        
        initComponents();
        welcomeText.setText("¡Hi, " + userName + "!");
        setLocationRelativeTo(null);
        setVisible(true);
        chatFrame = new ChatGUI(this);
        initChatFrame();
        new TCPListener( this ).start();
        
        udp = new UDPListener(this);
        try {
            udp.init();
            udp.start();
        } catch (SocketException e) {
            isUDPavailable = false;
        }
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        game1Button = new javax.swing.JToggleButton();
        game2Button = new javax.swing.JToggleButton();
        game3Button = new javax.swing.JToggleButton();
        game4Button = new javax.swing.JToggleButton();
        exitButton = new javax.swing.JButton();
        welcomeText = new javax.swing.JLabel();
        chatButton = new javax.swing.JToggleButton();
        nPlayersGame1 = new javax.swing.JLabel();
        nPlayersGame2 = new javax.swing.JLabel();
        nPlayersGame3 = new javax.swing.JLabel();
        tipLabel = new javax.swing.JLabel();
        startButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Online Chat & Games");
        setMinimumSize(new java.awt.Dimension(550, 450));
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("ONLINE CHAT & GAMES");

        game1Button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jorgebruned/fun4friends/Games/Pong/pong.png"))); // NOI18N
        game1Button.setText("Pong");
        game1Button.setToolTipText("Click to select/unselect Pong");
        game1Button.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        game1Button.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        game1Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                game1ButtonActionPerformed(evt);
            }
        });

        game2Button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jorgebruned/fun4friends/Games/BrickBreaker/brickbreaker.png"))); // NOI18N
        game2Button.setText("Wallbreaker");
        game2Button.setToolTipText("Click to select/unselect Wallbreaker");
        game2Button.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        game2Button.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        game2Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                game2ButtonActionPerformed(evt);
            }
        });

        game3Button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jorgebruned/fun4friends/Games/Pictionary/pictionary.png"))); // NOI18N
        game3Button.setText("Pictionary");
        game3Button.setToolTipText("Click to select/unselect Pictionary");
        game3Button.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        game3Button.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        game3Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                game3ButtonActionPerformed(evt);
            }
        });

        game4Button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jorgebruned/fun4friends/Games/videogame.png"))); // NOI18N
        game4Button.setText("More games");
        game4Button.setToolTipText("The escalability and abstraction of this multiplayer framework makes it perfect for adding more games with minimum extra developing");
        game4Button.setEnabled(false);
        game4Button.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        game4Button.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        game4Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                game4ButtonActionPerformed(evt);
            }
        });

        exitButton.setText("Exit");
        exitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitButtonActionPerformed(evt);
            }
        });

        welcomeText.setText("¡Hello, dear user!");

        chatButton.setSelected(true);
        chatButton.setText("Show/Hide Chat");
        chatButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chatButtonActionPerformed(evt);
            }
        });

        nPlayersGame1.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        nPlayersGame1.setForeground(new java.awt.Color(102, 102, 102));
        nPlayersGame1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nPlayersGame1.setText("0 players waiting");

        nPlayersGame2.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        nPlayersGame2.setForeground(new java.awt.Color(102, 102, 102));
        nPlayersGame2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nPlayersGame2.setText("0 players waiting");

        nPlayersGame3.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        nPlayersGame3.setForeground(new java.awt.Color(102, 102, 102));
        nPlayersGame3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nPlayersGame3.setText("0 players waiting");

        tipLabel.setForeground(new java.awt.Color(102, 102, 102));
        tipLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tipLabel.setText("Tip: place the mouse over the number of waiting players for a game to see their names!");
        tipLabel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        startButton.setText("Start selected game");
        startButton.setEnabled(false);
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(game1Button, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(game3Button, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nPlayersGame1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nPlayersGame3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(game4Button, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(game2Button, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nPlayersGame2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(welcomeText)
                        .addGap(31, 31, 31)
                        .addComponent(chatButton)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(tipLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(exitButton)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(welcomeText)
                    .addComponent(chatButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tipLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(game1Button)
                    .addComponent(game2Button))
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nPlayersGame1)
                    .addComponent(nPlayersGame2))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(game3Button)
                    .addComponent(game4Button))
                .addGap(0, 0, 0)
                .addComponent(nPlayersGame3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(exitButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void initChatFrame() {
        chatFrame.setVisible(true);
        relocateChatWindow( this );
        // Show and hide the chat when necessary
        this.addWindowStateListener(new WindowStateListener() {
            @Override
            public void windowStateChanged(WindowEvent e) {
                chatFrame.setVisible(e.getNewState() != Frame.ICONIFIED);
            }
        });
        chatFrame.addComponentListener(new ComponentAdapter(){
            @Override
            public void componentHidden(ComponentEvent e){
                chatButton.setSelected(false);
            }
            @Override
            public void componentShown(ComponentEvent e){
                chatButton.setSelected(true);
            }
        });
    }
    
    public void relocateChatWindow( JFrame reference ) {
        chatFrame.setLocationRelativeTo(reference);
        chatFrame.setSize(chatFrame.getSize().width, reference.getSize().height);
        chatFrame.setLocation(reference.getLocationOnScreen().x + reference.getSize().width,
                this.getLocationOnScreen().y);
    }
    
    public void display() {
        setVisible(true);
        relocateChatWindow(this);
    }
    
    public void setServer(boolean isServer) {
        if (isServer) {
            this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            this.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent event) {
                    if (JOptionPane.showConfirmDialog(null, "You are currently hosting a server\n" +
                            "If you exit, all the connected players will be kicked out\n" +
                            "Do you still want to close the app?",
                            "Confirm",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE)
                            == JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            });
        }
    }
    
    private void chatButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chatButtonActionPerformed
        chatFrame.setVisible( chatButton.isSelected() );
    }//GEN-LAST:event_chatButtonActionPerformed

    private void exitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitButtonActionPerformed
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_exitButtonActionPerformed

    private void game1ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_game1ButtonActionPerformed
        pickGame(game1Button.isSelected() ? Game.PONG : Game.NONE);
    }//GEN-LAST:event_game1ButtonActionPerformed

    private void game2ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_game2ButtonActionPerformed
        pickGame(game2Button.isSelected() ? Game.BRICK_BREAKER : Game.NONE);
    }//GEN-LAST:event_game2ButtonActionPerformed

    private void game3ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_game3ButtonActionPerformed
        pickGame(game3Button.isSelected() ? Game.PICTIONARY : Game.NONE);
    }//GEN-LAST:event_game3ButtonActionPerformed

    private void game4ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_game4ButtonActionPerformed
        pickGame(game4Button.isSelected() ? Game.PACHISI : Game.NONE);
    }//GEN-LAST:event_game4ButtonActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        try {
            sendTCP( new ControlMessage(ControlMessage.START_GAME) );
        } catch (IOException ex) {
            JOptionPane.showMessageDialog( null, "Couldn't start game", "Error",
                    JOptionPane.ERROR_MESSAGE );
        }
    }//GEN-LAST:event_startButtonActionPerformed
    
    public void sendTCP (Message msg) throws IOException {
        synchronized (out) {
            out.writeObject( msg );
        }
    }

    public Message receiveTCP() throws IOException, SocketException {
        synchronized (in) {
            try {
                Object msg;
                if ((msg = in.readObject()) instanceof Message)
                    return (Message)msg;
                else
                    return null;
            } catch (ClassNotFoundException e) {
                return null;
            }
        }
    }
    
    public void sendUDP (Message msg) throws IOException {
        if (isUDPavailable) {
            try {
                udp.sendUDP(msg);
            } catch (IOException e) {
                sendTCP(msg);
            }
        } else
            sendTCP(msg);
    }

    public String authenticate() throws IOException {
        String userName = askForName();
        Message msg = receiveTCP();
        if (!(msg instanceof ControlMessage) ||
                ((ControlMessage)msg).getAction() != ControlMessage.WELCOME) {
            JOptionPane.showMessageDialog(null, "Expected welcome message",
                    "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        sendTCP( new ControlMessage(ControlMessage.SET_NAME, new String[]{userName}) );
        msg = receiveTCP();
        if ( !(msg instanceof ControlMessage) ||
                ((ControlMessage)msg).getAction() != ControlMessage.AUTH_RESULT ) {
            JOptionPane.showMessageDialog(null, "Expected authentication result",
                    "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        while ( !((ControlMessage)msg).getParams()[0].equals("1") ) {
            userName = askForName(((ControlMessage)msg).getParams()[1]);
            sendTCP( new ControlMessage(ControlMessage.SET_NAME, new String[]{userName}) );
            msg = receiveTCP();
            if ( !(msg instanceof ControlMessage) ||
                    ((ControlMessage)msg).getAction() != ControlMessage.AUTH_RESULT ) {
                JOptionPane.showMessageDialog(null, "Expected authentication result",
                        "Error", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }
        }
        return userName;
    }
    
    public void messageReceived (ClientGUI mainFrame, Message msg) {
        if (msg instanceof ChatMessage)
            mainFrame.chatFrame.addMessage( (ChatMessage)msg );
        else if (mainFrame.gameExec != null && (msg instanceof GameControl))
            mainFrame.gameExec.update( (GameControl)msg );
        else if (mainFrame.gameExec != null && (msg instanceof GameStatus))
            mainFrame.gameExec.update( (GameStatus)msg );
        else if (msg instanceof ControlMessage) {
            switch ( ((ControlMessage)msg).getAction() ) {
                case ControlMessage.GAME_PICKED:
                    mainFrame.updateWaitingPlayers( ((ControlMessage)msg).getParams()[0],
                            ((ControlMessage)msg).getParams()[1]);
                    break;
                case ControlMessage.GAME_STARTED:
                    mainFrame.startGame();
                    break;
                case ControlMessage.GAME_ENDED:
                    mainFrame.endGame();
                    break;
                case ControlMessage.MUST_AUTH_UDP:
                    try {
                        udp.send(new ControlMessage(ControlMessage.UDP_AUTH, new String[]{userName}));
                    } catch (IOException ex) {
                        System.out.println("Couldn't send UDP AUTH!");
                        Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case ControlMessage.ERROR:
                    JOptionPane.showMessageDialog(null, ((ControlMessage)msg).getParams()[0],
                            "Error", JOptionPane.ERROR_MESSAGE);
                    break;
            }
        }
    }

    private String askForName() {
        if (Fun4Friends.UNDER_DEVELOPMENT)
            return "Jorge";
        String name = JOptionPane.showInputDialog(this,"Kindly enter your name",
                "Online Chat & Games",JOptionPane.QUESTION_MESSAGE);
        if (name == null)
            System.exit(0);
        while(name.isEmpty())
            if ((name = JOptionPane.showInputDialog(this,"Please, type a valid name",
                    "Online Chat & Games",JOptionPane.ERROR_MESSAGE)) == null)
                System.exit(0);
        return name;
    }
    
    private String askForName (String error) {
        String name = JOptionPane.showInputDialog(this, error,
                "Online Chat & Games",JOptionPane.ERROR_MESSAGE);
        if (name == null)
            System.exit(0);
        while(name.isEmpty())
            if ((name = JOptionPane.showInputDialog(this,"Please, type a valid name",
                    "Online Chat & Games",JOptionPane.ERROR_MESSAGE)) == null)
                System.exit(0);
        return name;
    }
    
    void startGame () {
        setVisible(false);
        gameExec = new ClientGameExecutor( this, selectedGame );
        gameExec.start();
        //System.out.println("Game started");
        pickGame( Game.NONE );
    }
    
    void endGame () {
        if (gameExec != null) {
            gameExec.gameEnded();
            gameExec = null;
            //System.out.println("Game ended");
            JOptionPane.showMessageDialog(null, "The game has finished", "Info",
                    JOptionPane.INFORMATION_MESSAGE);
            setVisible(true);
        }
    }
    
    private void pickGame (int nGame) {
        // Send request to the socketTCP
        try {
            sendTCP( new ControlMessage(ControlMessage.PICK_GAME, new String[]{nGame+""}) );
        } catch (IOException ex) {
            Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        selectedGame = nGame;
        
        // Update GUI (Toggle Buttons)
        startButton.setEnabled(nGame != Game.NONE);
        game1Button.setSelected(selectedGame == Game.PONG);
        game2Button.setSelected(selectedGame == Game.BRICK_BREAKER);
        game3Button.setSelected(selectedGame == Game.PICTIONARY);
        game4Button.setSelected(selectedGame == Game.PACHISI);
    }
    
    private void updateWaitingPlayers (String userName, String game) {
        
        int gameNum = Integer.parseInt(game);
        
        // Remove specified user from any other list he/she is in
        for (List<String> list : waitingPlayers.values())
            list.removeIf( u -> u.equals(userName) );
        if (gameNum != Game.NONE) {
            // Add user to the list of the game he has chosen
            if (waitingPlayers.containsKey(gameNum))
                waitingPlayers.get(gameNum).add( userName );
            else
                waitingPlayers.put(gameNum, new LinkedList( Arrays.asList(userName) ));   
        }
        
        /* Update number of waiting players in the GUI (if more games were added,
         * it would be a good idea to create arrays with the text labels and the
         * toggle buttons to update them using a loop) */
        nPlayersGame1.setText( (waitingPlayers.containsKey(Game.PONG) ?
                waitingPlayers.get(Game.PONG).size() : 0) + " players waiting");
        nPlayersGame1.setToolTipText(waitingPlayers.containsKey(Game.PONG) &&
                waitingPlayers.get(Game.PONG).size() > 0 ? "Players waiting: " +
                String.join(", ", waitingPlayers.get(Game.PONG)) : "No players waiting");
        nPlayersGame2.setText( (waitingPlayers.containsKey(Game.BRICK_BREAKER) ?
                waitingPlayers.get(Game.BRICK_BREAKER).size() : 0) + " players waiting");
        nPlayersGame2.setToolTipText(waitingPlayers.containsKey(Game.BRICK_BREAKER) &&
                waitingPlayers.get(Game.BRICK_BREAKER).size() > 0 ? "Players waiting: " +
                String.join(", ", waitingPlayers.get(Game.BRICK_BREAKER)) : "No players waiting");
        nPlayersGame3.setText( (waitingPlayers.containsKey(Game.PICTIONARY) ?
                waitingPlayers.get(Game.PICTIONARY).size() : 0) + " players waiting");
        nPlayersGame3.setToolTipText(waitingPlayers.containsKey(Game.PICTIONARY) &&
                waitingPlayers.get(Game.PICTIONARY).size() > 0 ? "Players waiting: " +
                String.join(", ", waitingPlayers.get(Game.PICTIONARY)) : "No players waiting");
    }
    
    private class TCPListener extends Thread {
        
        private final ClientGUI mainFrame;

        public TCPListener(ClientGUI frame) {
            this.mainFrame = frame;
        }

        @Override
        public void run() {
            
            try {
                Message msg;
                while ( (msg = receiveTCP()) != null )
                    messageReceived(mainFrame, msg);
            } catch (SocketException ex) {
                JOptionPane.showMessageDialog(null, "The server has closed the connection",
                        "Error", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            } catch (IOException ex) {
                Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
        }
        
    }
    
    private class UDPListener extends Thread {
        
        private final ClientGUI mainFrame;
        private DatagramSocket socketUDP;
        long lastUDPcommunication;
        boolean receiverWaiting = false;

        public UDPListener(ClientGUI frame) {
            this.mainFrame = frame;
        }
        
        public void init() throws SocketException {
            //socketUDP = new DatagramSocket();
            try {
                socketUDP = new DatagramSocket();
                //socketUDP.setSendBufferSize(UDPServer.BUFFER_LENGTH);
                reconnectIfNecessary();
            } catch (SocketException ex) {
                Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        private void reconnectIfNecessary() {
            if (System.currentTimeMillis() <= lastUDPcommunication + UDPServer.UDP_TIMEOUT * 1000)
                return;
            //System.out.println("Reconnecting UDP");
            //socketUDP.connect(socketTCP.getInetAddress(), Server.DEFAULT_PORT);
            //System.out.println("connected: "+socketUDP.isConnected()+"closed: "+socketUDP.isClosed());
            try {
                send(new ControlMessage(ControlMessage.UDP_AUTH, new String[]{userName}));
                //System.out.println("Reconnected UDP");
                lastUDPcommunication = System.currentTimeMillis();
            } catch (IOException ex) {
                //System.out.println("Interrupting UDP");
                Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
                this.interrupt();
                mainFrame.isUDPavailable = false;
            }
            //if (receiverWaiting)
                //this.notify();
        }
        
        private synchronized void send (Message msg) throws IOException {
            //synchronized (socketUDP) {
                final ByteArrayOutputStream byst = new ByteArrayOutputStream(UDPServer.BUFFER_LENGTH);
                final ObjectOutputStream udpOut = new ObjectOutputStream(byst);
                udpOut.writeObject(msg);
                final byte[] data = byst.toByteArray();
                //socketUDP.send( new DatagramPacket(data, UDPServer.BUFFER_LENGTH) );
                //System.out.println("Before sending");
                DatagramPacket pck = new DatagramPacket(data, data.length,
                        socketTCP.getInetAddress(), Server.DEFAULT_PORT);
                socketUDP.send(pck);
                //System.out.println("Sent UDP datagram");
                lastUDPcommunication = System.currentTimeMillis();
            //}
        }
        
        public synchronized void sendUDP (Message msg) throws IOException {
            //System.out.println("Sending UDP!");
            //if (!socketIsAvailable())
                reconnectIfNecessary();
            send(msg);
        }
        
        /*private boolean socketIsAvailable() {
            if (socketUDP == null)
                return false;
            long currentMillis = System.currentTimeMillis();
            if (currentMillis > lastUDPcommunication + UDPServer.UDP_TIMEOUT * 1000)
                socketUDP.close();
            return /*socketUDP.isConnected() && *///!socketUDP.isClosed();
        //}

        private Message receive() throws IOException, SocketException {
            /*while (!socketIsAvailable()) {
                receiverWaiting = true;
                try {
                    wait();
                } catch (InterruptedException ex) {}
            }*/
            //receiverWaiting = false;
            //synchronized (socketUDP) {
                DatagramPacket received = new DatagramPacket(new byte[BUFFER_LENGTH], BUFFER_LENGTH);
                socketUDP.receive(received);
                try {
                    Object msg = new ObjectInputStream(
                            new ByteArrayInputStream(received.getData())).readObject();
                    lastUDPcommunication = System.currentTimeMillis();
                    if (msg instanceof Message)
                        return (Message)msg;
                    else
                        throw new ClassNotFoundException("Message received through UDP "
                                + "does not implement Message");
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            //}
        }

        @Override
        public void run() {
            
            try {
                Message msg;
                while ( !interrupted() && (msg = receive()) != null )
                    messageReceived(mainFrame, msg);
            } catch (IOException ex) { // SocketException extends IOException
                Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
                socketUDP.close();
                socketUDP = null;
            }
                    
        }
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClientGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClientGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClientGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClientGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new ClientGUI(Fun4Friends.LOCAL_ADDRESS, Server.DEFAULT_PORT);
                } catch (IOException ex) { // UnknownHostException is a subclass of IOException
                    JOptionPane.showMessageDialog(null, "Couldn't connect to the server",
                                        "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton chatButton;
    private javax.swing.JButton exitButton;
    private javax.swing.JToggleButton game1Button;
    private javax.swing.JToggleButton game2Button;
    private javax.swing.JToggleButton game3Button;
    private javax.swing.JToggleButton game4Button;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel nPlayersGame1;
    private javax.swing.JLabel nPlayersGame2;
    private javax.swing.JLabel nPlayersGame3;
    private javax.swing.JButton startButton;
    private javax.swing.JLabel tipLabel;
    private javax.swing.JLabel welcomeText;
    // End of variables declaration//GEN-END:variables
}
