    Finally here is the final version of the project. I would like to remark a
couple of things:

    1. The Server and Client can be run independently, but the best option is to
run the main class (Fun4Friends.java), which connects to the default server (my
IP address) and, if it's not available, it lets you host a server or join one.

    2. I have focused, as you have seen through Slack, in the framework, rather
than in the games, taking abstraction as far as I have been able to.

    3. For Pictionary, the server sends the image already rendered to the clients
instead of messages like "DRAW size color x y". I have done that since I wanted
to try to stream images in real time, which I think I have achieved successfully
(even though it might not be the optimal way of implementing it). Another reason
why I have done it like this is to guarantee that every client gets exactly the
same drawing (the smoothing I have included may produce different outputs if the
time interval between updates is different).

    4. In this last phase I have finally been able to implement UDP communication
for real-time game updates (of course, TCP is still used for all other things and
important messages). The way I have carried it out allows UDP communication to
work online without the need of the client having any open ports (the server, of
course, must have the default port, 5678, open for both TCP and UDP).

    Best regards,
    Jorge Bruned