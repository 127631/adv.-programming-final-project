/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games;

import java.io.Serializable;

/**
 * This class is used to store and transform two-dimensional vectors
 * @author Jorge Bruned Alamán
 */
public class Vector2 implements Serializable {
    
    public double x,y;

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    
    public void move(Vector2 speed, double seconds) {
        x += speed.x * seconds;
        y += speed.y * seconds;
    }
    
    public double modulus() {
        return Math.sqrt(x*x + y*y);
    }

    public void normalize() {
        final double modulus = modulus();
        x /= modulus;
        y /= modulus;
    }

    public void multiply(double factor) {
        x *= factor;
        y *= factor;
    }
    
}
