/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.Pictionary;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import jorgebruned.fun4friends.Games.GameEntity;

/**
 * Stores the current drawing of a Pictionary game.
 * Methods to encode as byte[] and decode back are implemented here.
 * @author Jorge Bruned Alamán
 */
public class Drawing extends GameEntity {

    // Constants
    public static final int WIDTH = 700;
    public static final String COMPRESSION = "png";
    /* jpeg decoding is much quicker but when the drawing is complex, it weighs
     * too much for UDP. It also has worse quality so png is not a bad choice either */

    transient BufferedImage drawing; // BufferedImages aren't serializable: transient
    byte[] data; // therefore, I send them through the socket using byte[] instead
    private transient Graphics2D g2; // to avoid calling createGraphics() all the time
    
    /*
     * These two variables are used to smooth the drawing. If the delta time since
     * the last call to draw() is small, a line will be created instead of separate
     * ovals. This way, if the CPU is slow or the player draws very quickly, we
     * won't get separate ovals
     */
    transient private long lastDrawn = 0; // obtainted using currentTimeMillis
    transient private int lastX, lastY;
    
    public Drawing() {
        drawing = new BufferedImage(WIDTH, (int)(WIDTH/Pictionary.ASPECT_RATIO),
                BufferedImage.TYPE_INT_ARGB_PRE);
    }
    
    public void init() {
        g2 = getGraphics2D();
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, drawing.getWidth(), drawing.getHeight()/2);
    }
    
    public void draw(int x, int y, Color color, int diameter) {
        g2.setColor( color );
        g2.setStroke( new BasicStroke(diameter,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND) );
        if (lastDrawn > System.currentTimeMillis() - 100)
            g2.drawLine(x, y, lastX, lastY);
        else
            g2.fillOval((int)(x - diameter*0.5), (int)(y - diameter*0.5), diameter, diameter);
        lastX = x;
        lastY = y;
        lastDrawn = System.currentTimeMillis();
    }
    
    public Graphics2D getGraphics2D() {
        return drawing.createGraphics();
    }
    
    public BufferedImage decodeBytes() {
        return drawing = getImageFromBytes(data);
    }
    
    public byte[] encodeImage() {
        return data = getBytesFromImage(drawing);
    }
    
    public static byte[] getBytesFromImage(BufferedImage image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.setUseCache(false);
            ImageIO.write(image, COMPRESSION, baos);
            baos.flush();
        } catch (IOException ex) {
            Logger.getLogger(Drawing.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return baos.toByteArray();
    }
    
    public static BufferedImage getImageFromBytes(byte[] bytes) {
        try {
            if (bytes != null) {
                ImageIO.setUseCache(false);
                BufferedImage img = ImageIO.read(new ByteArrayInputStream(bytes));
                return img;
            } else return null;
        } catch (IOException ex) {
            Logger.getLogger(Drawing.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, panelWidth, panelHeight);
        if (drawing == null && data != null)
            decodeBytes();
        if (drawing != null)
            g2.drawImage(drawing.getScaledInstance(panelWidth, panelHeight,
                    Image.SCALE_DEFAULT), 0, 0, null);
    }
    
}
