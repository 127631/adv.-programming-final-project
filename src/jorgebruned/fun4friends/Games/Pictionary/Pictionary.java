/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.Pictionary;

import java.awt.Color;
import jorgebruned.fun4friends.Games.Game;
import jorgebruned.fun4friends.Games.GameEntity;
import jorgebruned.fun4friends.Games.NotRealTimeGameException;
import jorgebruned.fun4friends.Messages.ChatMessage;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.GameStatus;
import jorgebruned.fun4friends.Server.ServerGameExecutor;

/**
 * This Game subclass simulates a Pictionary game.<br><br>
 * 
 * Probably it would be better in terms of data traffic to send to the clients
 * a GameControl message like "PAINT color size x y" as well, but I wanted to
 * to try and send images in real time; and also guarantee that every client
 * gets the exact same drawing (the smoothing may produce different outputs
 * if the time interval between updates is different)
 * 
 * @author Jorge Bruned Alamán
 */
public class Pictionary extends Game {
    
    public static final int MIN_PLAYERS = 2;
    public static final int MAX_PLAYERS = 10;
    public static final int FPS = 30; // since I'm sending images in real time
    public static final int WAITING_TIME = 0; // Pictionary doesn't need any wait before the game starts
    
    // GameControl actions
    public static final int DRAW = 0;
    public static final int GUESS = 1;
    public static final int TURN_DRAW = 2;
    public static final int TURN_GUESS = 3;
    
    // Colors and words
    public static final int BLACK = 0;
    public static final int RED = 1;
    public static final int GREEN = 2;
    public static final int BLUE = 3;
    public static final int YELLOW = 4;
    public static final int PINK = 5;
    public static final int ORANGE = 6;
    public static final int WHITE = 7;
    public static final Color[] COLORS = {Color.BLACK, Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.MAGENTA, Color.ORANGE, Color.WHITE};
    public static final String[] WORDS = {"pants","star","cube","box","grapes","bark","bread","dog","fork","clock","triangle","mouse","helicopter","butterfly","pillow","daisy","orange","nose","nail","elephant","coin","coat","backpack","car","mountains","apple","robot","pie","train","dinosaur","pencil","bathroom","cookie","shoe","crack","kite","bear","banana","desk","arm","oval","cheese","hook","cat","doll","tail","book","spider","snake","rain","snowflake","neck","Earth","owl","window","woman","boat","bounce","popsicle","flower","cow","lion","blocks","rabbit","rock","button","legs","egg","bunk bed","king","family","pizza","baby","island","pig","fire","key","alligator","bell","jail","ocean","motorcycle","caterpillar","carrot","comb"};

    // Game constants
    public static final int MAX_GUESSES = 20;
    public static final int MAX_BRUSH_SIZE = 80;
    public static final float ASPECT_RATIO = 4/(float)3;
    
    private int guessesLeft;
    private int playerDrawing = 0;
    private String currentWord;
    private Drawing drawing;

    public Pictionary(ServerGameExecutor ge) {
        super(ge);
    }
    
    @Override
    public void init() {
        drawing = new Drawing();
        drawing.init();
        guessesLeft = MAX_GUESSES;
        currentWord = WORDS[(int)(Math.random() * WORDS.length)];
        playerDrawing = (playerDrawing+1) % ge.nPlayers();
        ge.sendToAllPlayersExcept( new GameControl(TURN_GUESS, new String[]{ge.getPlayerName(playerDrawing)}), playerDrawing );
        ge.sendToPlayer( new GameControl(TURN_DRAW, new String[]{currentWord}), playerDrawing );
    }
    
    @Override
    public int minPlayers() {
        return MIN_PLAYERS;
    }

    @Override
    public int maxPlayers() {
        return MAX_PLAYERS;
    }

    @Override
    public int getFPS() {
        return FPS;
    }
    
    @Override
    public int getWaitingTimeBeforeStart() {
        return WAITING_TIME;
    }
    
    @Override
    public void logic (int nPlayer, GameControl action) {
        if (nPlayer == playerDrawing && action.getAction() == DRAW) {
            final String[] params = action.getParams();
            drawing.draw(Integer.parseInt(params[2]), Integer.parseInt(params[3]),
                    COLORS[Integer.parseInt(params[0])],
                    (int)(MAX_BRUSH_SIZE * Integer.parseInt(params[1])/(float)100));
        } else if (action.getAction() == GUESS) {
            final String guess = action.getParams()[0];
            ge.sendToAllPlayers( new ChatMessage(ge.getPlayerName(nPlayer) +
                    " guessed " + guess, "[Pictionary]") );
            if (guess.equalsIgnoreCase(currentWord) || --guessesLeft == 0) {
                ge.sendToAllPlayers( new ChatMessage("The word was " +
                        currentWord + "!", "[Pictionary]") );
                init();
            }
        }
    }

    @Override
    public void playerRemoved(int nPlayer) {
        if (nPlayer == playerDrawing)
            init();
        super.playerRemoved(nPlayer);
    }

    @Override
    public GameStatus getStatus(double dt) throws NotRealTimeGameException {
        if (drawing != null)
            drawing.encodeImage();
        return new GameStatus( new GameEntity[]{drawing} );
    }
    
}
