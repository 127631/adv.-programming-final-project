/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games;

/**
 * Exception thrown when a game which doesn't need to send real-time updates
 * is requested a game update by a game executor
 * @author Jorge Bruned Alamán
 */
public class NotRealTimeGameException extends Exception {}
