/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.Pong;

import jorgebruned.fun4friends.Games.Game;
import jorgebruned.fun4friends.Games.GameEntity;
import jorgebruned.fun4friends.Messages.GameStatus;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Server.ServerGameExecutor;

/**
 * This class simulates a Pong game.
 * It must be executed using a GameExecutor
 * @author Jorge Bruned Alamán
 */
public class Pong extends Game {
    
    public static final int MIN_PLAYERS = 2;
    public static final int MAX_PLAYERS = 2;
    
    public static final int MOVE_PADDLE = 0;
    public static final int SCORED = 1;
    
    public static final int LEFT_PADDLE_ID = 0;
    public static final int RIGHT_PADDLE_ID = 1;
    public static final int BALL_ID = 2;
    
    public static final float ASPECT_RATIO = 16/(float)9;
    
    public static final int SCORE_RESIZE_PADDLE = 4;
    public static final int SCORE_SPEED_BALL = 5;
        
    private final Ball ball;
    private final Paddle paddle1, paddle2;
    private final int[] scores = new int[]{0,0};

    public Pong(ServerGameExecutor ge) {
        super(ge);
        paddle1 = new Paddle(LEFT_PADDLE_ID);
        paddle2 = new Paddle(RIGHT_PADDLE_ID);
        ball = new Ball();
    }

    @Override
    public int minPlayers() {
        return MIN_PLAYERS;
    }

    @Override
    public int maxPlayers() {
        return MAX_PLAYERS;
    }

    @Override
    public void init(){
        ball.initPos();
        paddle1.initPos();
        paddle2.initPos();
    }
    
    private void updateStatus (double dt) {
        paddle1.updatePos(dt);
        paddle2.updatePos(dt);
        ball.updatePos(dt);
        if (ball.pos.x < Paddle.DIST_TO_BORDER + Paddle.WIDTH) {
            if (paddle1.yPos <= ball.pos.y + Ball.DIAMETER/* * 0.5 */ && paddle1.yPos + paddle1.height >= ball.pos.y/* + Ball.DIAMETER*0.5*/) {
                if (ball.vel.x < 0)
                    ball.xDirSwitch();
            } else
                scored(2);
        } else if (ball.pos.x + Ball.DIAMETER > ASPECT_RATIO - Paddle.DIST_TO_BORDER) {
            if (paddle2.yPos <= ball.pos.y && paddle2.yPos + paddle2.height >= ball.pos.y + Ball.DIAMETER) {
                if (ball.vel.x > 0)
                    ball.xDirSwitch();
            } else
                scored(1);
        }
        if (ball.pos.y < 0 || ball.pos.y + Ball.DIAMETER > 1)
            ball.yDirSwitch();
    }
    
    private void scored (int nPlayer) {
        init();
        scores[nPlayer-1]++;
        if (nPlayer == 1 && scores[0]%SCORE_RESIZE_PADDLE == 0)
            paddle1.decreaseHeight(0.02);
        else if (nPlayer == 2 && scores[1]%SCORE_RESIZE_PADDLE == 0)
            paddle2.decreaseHeight(0.02);
        if ((scores[0]+scores[1])%SCORE_SPEED_BALL == 0)
            ball.increaseSpeed(0.05);
    }
    
    @Override
    public void logic (int nPlayer, GameControl action) {
        if (action.getAction() == MOVE_PADDLE) {
            final int dir = Integer.parseInt( action.getParams()[0] );
            if (nPlayer == 0)
                paddle1.yDir = dir;
            else if (nPlayer == 1)
                paddle2.yDir = dir;
        }
    }
    
    @Override
    public GameStatus getStatus(double dt) {
        updateStatus(dt);
        return new PongStatus( new GameEntity[]{ball,paddle1,paddle2},
                scores[0], scores[1] );
    }
    
}
