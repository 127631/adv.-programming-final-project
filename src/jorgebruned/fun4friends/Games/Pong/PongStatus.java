/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.Pong;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import jorgebruned.fun4friends.Games.GameEntity;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.GameStatus;

/**
 * This class includes all the information needed to represent the current
 * status of a Pong game, and the method to paint it
 * @author Jorge Bruned Alamán
 */
public class PongStatus extends GameStatus {
    
    int score1, score2;

    public PongStatus(GameEntity[] entities, int score1, int score2) {
        super(entities);
        this.score1 = score1;
        this.score2 = score2;
    }

    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        // Draw entities
        super.draw(g2, panelWidth, panelHeight);

        // Draw specific Pong elements
        g2.setColor(Color.WHITE);
        g2.setStroke(new BasicStroke(3, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0));
        g2.drawLine(panelWidth/2, 0, panelWidth/2, panelHeight);
        g2.setFont( new Font("Arial", Font.BOLD, panelHeight/20) );
        g2.drawString( score1 > 9 ? " " + score1 : "" + score1,
                (int)(0.43 * panelWidth), panelHeight/10);
        g2.drawString( "" + score2, (int)(0.55 * panelWidth), panelHeight/10);
    }
    
    @Override
    public GameControl getUpdate (int xDir, int yDir) {
        return new GameControl ( Pong.MOVE_PADDLE, new String[]{yDir+""} );
    }
    
}
