/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.Pong;

import java.awt.Color;
import java.awt.Graphics2D;
import jorgebruned.fun4friends.Games.GameEntity;

/**
 * This class implements a paddle for playing Pong
 * @author Jorge Bruned Alamán
 */
public class Paddle extends GameEntity {
    
    private static final double SPEED = 1;
    public static final double DIST_TO_BORDER = 0.1;
    public static final double WIDTH = 0.02;
    private static final double INITIAL_HEIGHT = 0.3;
    private static final double MIN_HEIGHT = 0.15;
    double yPos;
    transient int yDir;
    double height = INITIAL_HEIGHT;
    int id;

    public Paddle(int id) {
        this.id = id;
        initPos();
    }

    public void initPos() {
        yPos = 0.5 - 0.5 * height;
        yDir = 0;
    }

    public double getY() {
        return yPos;
    }

    public double getHeight() {
        return height;
    }

    protected void updatePos(double dt) {
        if (yDir == 0)
            return;
        if ((yPos += SPEED * dt * yDir) < 0)
            yPos = 0;
        else if (yPos > 1 - height)
            yPos = 1 - height;
    }

    protected void decreaseHeight(double dh) {
        if ((height -= dh) < MIN_HEIGHT)
            height = MIN_HEIGHT;
    }

    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        final int widthByAspectRatio = (int)(panelWidth / Pong.ASPECT_RATIO);
        g2.setColor(Color.WHITE);
        if (id == Pong.LEFT_PADDLE_ID)
            g2.fillRect((int)(DIST_TO_BORDER * widthByAspectRatio), (int)(yPos * panelHeight),
                    (int)(WIDTH * widthByAspectRatio), (int)(height * panelHeight));
        else if (id == Pong.RIGHT_PADDLE_ID)
            g2.fillRect(panelWidth - (int)(DIST_TO_BORDER * widthByAspectRatio), (int)(yPos * panelHeight),
                    (int)(WIDTH * widthByAspectRatio), (int)(height * panelHeight));
    }
    
}
