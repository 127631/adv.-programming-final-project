/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.Pong;

import java.awt.Color;
import java.awt.Graphics2D;
import jorgebruned.fun4friends.Games.GameEntity;
import jorgebruned.fun4friends.Games.Vector2;

/**
 * This class implements a Ball for playing Pong
 * @author Jorge Bruned Alamán
 */
public class Ball extends GameEntity {
    
    public static final double DIAMETER = 0.03;
    public static final double MAX_SPEED = 1.8;
    public static final double BOUNCE_RANDOMNESS = 0.5;
    Vector2 pos;
    transient Vector2 vel; // I don't want to serialize the velocity
    transient double speed = 1.2;

    public Ball() {
        initPos();
    }

    public void initPos() {
        this.pos = new Vector2(0.5 * (Pong.ASPECT_RATIO - DIAMETER), 0.5 - 0.5 * DIAMETER);
        this.vel = new Vector2(0.5 * Math.random() + 0.5, 0.5 * (Math.random() - 0.5));
        vel.normalize();
        vel.multiply(speed);
        if (Math.random() >= 0.5)
            xDirSwitch();
    }

    public void updatePos(double dt) {
        pos.move(vel, dt);
    }

    public void xDirSwitch() {
        vel.x = -vel.x;
        vel.y += -BOUNCE_RANDOMNESS + Math.random()*BOUNCE_RANDOMNESS*2;
        vel.normalize();
        vel.multiply(speed);
    }

    public void yDirSwitch() {
        vel.y = -vel.y;
    }

    public void increaseSpeed(double ds) {
        speed += ds;
        if (speed > MAX_SPEED) {
            speed = MAX_SPEED;
        }
    }

    public Vector2 getPos() {
        return pos;
    }

    public Vector2 getVel() {
        return vel;
    }

    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        final int widthByAspectRatio = (int)(panelWidth / Pong.ASPECT_RATIO);
        g2.setColor(Color.WHITE);
        g2.fillOval((int)(pos.x * widthByAspectRatio), (int)(pos.y * panelHeight),
                (int)(DIAMETER * widthByAspectRatio), (int)(DIAMETER * panelHeight));
    }
    
}
