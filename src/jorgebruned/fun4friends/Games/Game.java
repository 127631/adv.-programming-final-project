/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games;

import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.GameStatus;
import jorgebruned.fun4friends.Server.ServerGameExecutor;

/**
 * Every game extends from this abstract class (only instantiated on the server,
 * only static members used on the client)
 * @author Jorge Bruned Alamán
 */
public abstract class Game {
    
    public static final int NONE = -1;
    public static final int PONG = 0;
    public static final int BRICK_BREAKER = 1;
    public static final int PICTIONARY = 2;
    public static final int PACHISI = 3;
    /**
     * Number of updates sent per second in real-time games
     * (default value, can be overriden in subclasses)
     */
    public static final int DEFAULT_FPS = 60;
    /**
     * Amount of seconds the game executor sleeps before starting the game
     */
    public static final int TIME_BEFORE_START = 3;
    
    boolean gameOver = false;

    public static boolean isRealTime(int gameNum) {
        return gameNum == PONG || gameNum == BRICK_BREAKER || gameNum == PICTIONARY;
    }
    
    public final ServerGameExecutor ge;
    
    public Game (ServerGameExecutor ge) {
        this.ge = ge;
    }
    
    public void playerRemoved(int nPlayer) {
        if (ge.nPlayers() < minPlayers())
            ge.endGame();
    };
    public abstract int minPlayers();
    public abstract int maxPlayers();
    public abstract void init();
    public abstract void logic(int nPlayer, GameControl action);
    public abstract GameStatus getStatus(double dt) throws NotRealTimeGameException;

    public int getFPS() {
        return DEFAULT_FPS;
    }
    
    public int getWaitingTimeBeforeStart() {
        return TIME_BEFORE_START;
    }
    
    public void end() {
        gameOver = true;
    }

    public boolean isOver() {
        return gameOver;
    }
    
}
