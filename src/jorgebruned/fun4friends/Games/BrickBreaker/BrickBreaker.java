/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.BrickBreaker;

import java.util.LinkedList;
import java.util.List;
import jorgebruned.fun4friends.Games.Game;
import jorgebruned.fun4friends.Games.GameEntity;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.GameStatus;
import jorgebruned.fun4friends.Server.ServerGameExecutor;

/**
 * Simulates a BrickBreaker game
 * @author Jorge Bruned Alamán
 */
public class BrickBreaker extends Game {
    
    public static final int MIN_PLAYERS = 2;
    public static final int MAX_PLAYERS = 2;
    
    // GameControl actions
    public static final int MOVE_PADDLE = 0;
    public static final int LIVES_LEFT = 1;
    
    // Paddle ids (to distiguish left and right ones)
    public static final int LEFT_PADDLE_ID = 0;
    public static final int RIGHT_PADDLE_ID = 1;
    
    // Every position is in the range ([0,1], [0,ASPECT_RATIO])
    public static final float ASPECT_RATIO = 4/(float)3;
    
    // Gameplay constants
    public static final int SCORE_RESIZE_PADDLE = 30;
    public static final int SCORE_SPEED_BALL = 30;
    public static final int INITIAL_LIVES = 5;
    public static final int BRICKS_PER_ROW = 4;
    public static final int NUMBER_OF_ROWS = 5;

    // Game entities and status variables
    private final Ball ball;
    private final Paddle paddle1, paddle2;
    private final List<Brick> bricks = new LinkedList<>();
    private int lives = INITIAL_LIVES;
    private int hitBricks = 0;

    public BrickBreaker(ServerGameExecutor ge) {
        super(ge);
        paddle1 = new Paddle(LEFT_PADDLE_ID);
        paddle2 = new Paddle(RIGHT_PADDLE_ID);
        ball = new Ball();
    }
    
    @Override
    public void init() {
        for (int i=0; i<NUMBER_OF_ROWS; i++) {
            for (int j=0; j<BRICKS_PER_ROW; j++)
                bricks.add( new Brick(i,j) );
            if (i%2 == 1)
                bricks.add( new Brick(i,BRICKS_PER_ROW) );
        }
        bricks.removeIf( Brick::isDestroyed );
    }

    @Override
    public int minPlayers() {
        return MIN_PLAYERS;
    }

    @Override
    public int maxPlayers() {
        return MAX_PLAYERS;
    }

    private void lostPoint() {
        if (lives-- == 0) // not --lives since when you have 0 balls left you can still play with the current one
            ge.endGame();
        ball.initPos();
        paddle1.initPos();
        paddle2.initPos();
    }
    
    private void updateStatus (double dt) {
        boolean ballBouncedX = ball.justBouncedX, ballBouncedY = ball.justBouncedY;
        paddle1.updatePos(dt);
        paddle2.updatePos(dt);
        ball.updatePos(dt);
        if (ball.pos.y > 1 - Paddle.DIST_TO_BOTTOM - Paddle.HEIGHT) {
            if ((ball.pos.x + Ball.DIAMETER > paddle1.xPos && ball.pos.x < paddle1.xPos + paddle1.width) || 
                    (ball.pos.x + Ball.DIAMETER > paddle2.xPos && ball.pos.x < paddle2.xPos + paddle2.width))
                ball.yDirSwitch();
            else
                lostPoint();
        }
        if (ball.pos.y < 0)
            ball.yDirSwitch();
        else if (ball.pos.x < 0 || ball.pos.x + Ball.DIAMETER > ASPECT_RATIO)
            ball.xDirSwitch();
        synchronized (bricks) {
            for (Brick b : bricks) {
                if (b.isCollidedBy( ball )) {
                    brickHit();
                    b.hit(Ball.DAMAGE);
                    if (!b.matchesBallPosX)
                        ball.xDirSwitch();
                    else if (!b.matchesBallPosY)
                        ball.yDirSwitch();
                }
                b.updateRelativeBallPosition( ball );
            }
            bricks.removeIf( Brick::isDestroyed );
        }
        if (bricks.isEmpty()) {
            init();
            ball.initPos();
        }
        if (ballBouncedX)
            ball.justBouncedX = false;
        if (ballBouncedY)
            ball.justBouncedY = false;
    }
    
    private void brickHit() {
        if (++hitBricks%SCORE_RESIZE_PADDLE == 0) {
            paddle1.decreaseWidth(0.02);
            paddle2.decreaseWidth(0.02);
        }
        if (hitBricks%SCORE_SPEED_BALL == 0)
            ball.increaseSpeed(0.08);
    }
    
    @Override
    public void logic (int nPlayer, GameControl action) {
        if (action.getAction() == MOVE_PADDLE) {
            final int dir = Integer.parseInt( action.getParams()[0] );
            if (nPlayer == 0)
                paddle1.xDir = dir;
            else if (nPlayer == 1)
                paddle2.xDir = dir;
        }
    }

    @Override
    public GameStatus getStatus(double dt) {
        updateStatus(dt);
        List<GameEntity> ges = new LinkedList(bricks);
        ges.add(ball);
        ges.add(paddle1);
        ges.add(paddle2);
        return new BrickBreakerStatus( ges.toArray(new GameEntity[]{}), lives );
    }

    
}
