/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.BrickBreaker;

import java.awt.Color;
import java.awt.Graphics2D;
import jorgebruned.fun4friends.Games.GameEntity;

/**
 * Implements the functionality of a BrickBreaker paddle
 * @author Jorge Bruned Alamán
 */
public class Paddle extends GameEntity {
    
    private static final double SPEED = 1;
    public static final double DIST_TO_BOTTOM = 0.1;
    public static final double HEIGHT = 0.02;
    private static final double INITIAL_WIDTH = 0.25;
    private static final double MIN_WIDTH = 0.1;
    double xPos;
    transient int xDir;
    double width = INITIAL_WIDTH;
    int id;

    public Paddle(int id) {
        this.id = id;
        initPos();
    }

    public void initPos() {
        xPos = (id == BrickBreaker.LEFT_PADDLE_ID ? 0.25 : 0.75) *
                BrickBreaker.ASPECT_RATIO - 0.5 * width;
        xDir = 0;
    }

    public double getX() {
        return xPos;
    }

    public double getWidth() {
        return width;
    }

    protected void updatePos(double dt) {
        if (xDir == 0)
            return;
        if ((xPos += SPEED * dt * xDir) < 0)
            xPos = 0;
        else if (xPos > BrickBreaker.ASPECT_RATIO - width)
            xPos = BrickBreaker.ASPECT_RATIO - width;
    }

    protected void decreaseWidth(double dw) {
        if ((width -= dw) < MIN_WIDTH)
            width = MIN_WIDTH;
    }

    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        final int widthByAspectRatio = (int)(panelWidth / BrickBreaker.ASPECT_RATIO);
        g2.setColor(Color.WHITE);
        g2.fillRect((int)(xPos * widthByAspectRatio), (int)(panelHeight * (1 - DIST_TO_BOTTOM)),
                (int)(width * widthByAspectRatio), (int)(Paddle.HEIGHT * panelHeight));
    }
    
}
