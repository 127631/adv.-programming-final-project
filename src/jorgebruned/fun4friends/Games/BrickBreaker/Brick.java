/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.BrickBreaker;

import java.awt.Color;
import java.awt.Graphics2D;
import jorgebruned.fun4friends.Games.GameEntity;
import jorgebruned.fun4friends.Games.Vector2;

/**
 * Simulates a BrickBreaker brick
 * @author Jorge Bruned Alamán
 */
public class Brick extends GameEntity {
    
    public static final double GAP_SIZE = 0.02;
    public static final double BRICKS_MAX_Y = 0.6;
    public static final double HEIGHT = (BRICKS_MAX_Y - GAP_SIZE *
            (BrickBreaker.NUMBER_OF_ROWS + 1)) / (double)BrickBreaker.NUMBER_OF_ROWS;
    public static final double FULL_WIDTH = (BrickBreaker.ASPECT_RATIO - GAP_SIZE *
            (BrickBreaker.BRICKS_PER_ROW + 1)) / (double)BrickBreaker.BRICKS_PER_ROW;
    private static final int MAX_LEVEL = 3;
    
    int level; // from 0 (destroyed) to MAX_LEVEL
    final Vector2 pos; // position of the top left corner
    final double width;
    transient boolean matchesBallPosX; // true if the x pos of the ball matches this brick's
    transient boolean matchesBallPosY; // same for y

    public Brick(int rowNum, int colNum) {
        
        // probability of not existing and being max level is the half of being level 1 or 2
        level = (int) (0.5 + MAX_LEVEL * Math.random());
        
        // init position according to the row and column numbers
        if (rowNum % 2 == 0) {
            pos = new Vector2(GAP_SIZE + colNum * (FULL_WIDTH + GAP_SIZE), GAP_SIZE + rowNum * (HEIGHT + GAP_SIZE));
            width = FULL_WIDTH;
        } else if (colNum == 0) {
            pos = new Vector2(0, GAP_SIZE + rowNum * (HEIGHT + GAP_SIZE));
            width = 0.5 * (FULL_WIDTH - GAP_SIZE);
        } else if (colNum == 5) {
            width = 0.5 * (FULL_WIDTH - GAP_SIZE);
            pos = new Vector2(BrickBreaker.ASPECT_RATIO - width, GAP_SIZE + rowNum * (HEIGHT + GAP_SIZE));
        } else {
            pos = new Vector2(0.5 * (FULL_WIDTH + GAP_SIZE) + (colNum - 1) * (FULL_WIDTH + GAP_SIZE), GAP_SIZE + rowNum * (HEIGHT + GAP_SIZE));
            width = FULL_WIDTH;
        }
    }

    public int getLevel() {
        return level;
    }

    public Vector2 getPos() {
        return pos;
    }

    public double getWidth() {
        return width;
    }

    public boolean isDestroyed() {
        return level == 0;
    }

    public boolean isCollidedBy(Ball ball) {
        final double xBall = ball.getPos().x;
        final double yBall = ball.getPos().y;
        return xBall + Ball.DIAMETER >= pos.x && xBall <= pos.x + width && yBall + Ball.DIAMETER >= pos.y && yBall <= pos.y + HEIGHT;
    }

    public void updateRelativeBallPosition(Ball ball) {
        final double xBall = ball.getPos().x;
        final double yBall = ball.getPos().y;
        matchesBallPosX = xBall + Ball.DIAMETER >= pos.x && xBall <= pos.x + width;
        matchesBallPosY = yBall + Ball.DIAMETER >= pos.y && yBall <= pos.y + HEIGHT;
    }

    public void hit(int damage) {
        level -= damage;
    }

    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        if (level <= 0)
            return;
        final int widthByAspectRatio = (int)(panelWidth / BrickBreaker.ASPECT_RATIO);
        g2.setColor(level == 1 ? Color.GREEN : level == 2 ? Color.YELLOW : Color.RED);
        g2.fillRect((int)(pos.x * widthByAspectRatio), (int)(pos.y * panelHeight),
                (int)(width * widthByAspectRatio), (int)(HEIGHT * panelHeight));
    }
    
}
