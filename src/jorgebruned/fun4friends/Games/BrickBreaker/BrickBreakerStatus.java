/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.BrickBreaker;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import jorgebruned.fun4friends.Games.GameEntity;
import jorgebruned.fun4friends.Messages.GameControl;
import jorgebruned.fun4friends.Messages.GameStatus;

/**
 * This class includes all the information needed to represent the current
 * status of a Pong game, and the method to paint it
 * @author Jorge Bruned Alamán
 */
public class BrickBreakerStatus extends GameStatus {
    
    int ballsLeft;

    public BrickBreakerStatus(GameEntity[] entities, int ballsLeft) {
        super(entities);
        this.ballsLeft = ballsLeft;
    }

    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        // Draw entities
        super.draw(g2, panelWidth, panelHeight);

        // Draw specific BrickBreaker elements
        g2.setColor(Color.WHITE);
        g2.setFont( new Font("Arial", Font.BOLD, panelHeight/20) );
        g2.drawString( "Balls left: " + ballsLeft,
                (int)(0.02 * panelWidth), (int)(0.05 * panelHeight));
    }
    
    @Override
    public GameControl getUpdate (int xDir, int yDir) {
        return new GameControl ( BrickBreaker.MOVE_PADDLE, new String[]{xDir+""} );
    }
    
}
