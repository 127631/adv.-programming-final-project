/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games.BrickBreaker;

import java.awt.Color;
import java.awt.Graphics2D;
import jorgebruned.fun4friends.Games.GameEntity;
import jorgebruned.fun4friends.Games.Vector2;

/**
 * Implements a BrickBreaker ball
 * @author Jorge Bruned Alamán
 */
public class Ball extends GameEntity {
    
    public static final double DIAMETER = 0.03;
    public static final double MAX_SPEED = 2;
    public static final double BOUNCE_RANDOMNESS = 0.3;
    public static final double INITIAL_SPEED = 0.8;
    public static final int DAMAGE = 1;
    
    Vector2 pos;
    transient Vector2 vel; // I don't want to serialize the velocity: transient
    transient double speed = INITIAL_SPEED;
    transient boolean justBouncedX = false, justBouncedY = false;

    public Ball() {
        initPos();
    }

    public void initPos() {
        this.pos = new Vector2(0.25 + 0.5 * (int)(Math.random() * 2) *
                (BrickBreaker.ASPECT_RATIO - DIAMETER), 1 - Paddle.DIST_TO_BOTTOM - DIAMETER);
        this.vel = new Vector2(Math.random() - 0.5, -0.5 * Math.random() - 0.3);
        vel.normalize();
        vel.multiply(speed);
    }

    public void updatePos(double dt) {
        pos.move(vel, dt);
    }

    public void xDirSwitch() {
        if (justBouncedX)
            return;
        vel.x = -vel.x;
        justBouncedX = true;
    }

    public void yDirSwitch() {
        if (justBouncedY)
            return;
        vel.y = -vel.y;
        vel.x += -BOUNCE_RANDOMNESS + Math.random()*BOUNCE_RANDOMNESS*2;
        vel.normalize();
        vel.multiply(speed);
        justBouncedY = true;
    }

    public void increaseSpeed(double ds) {
        speed += ds;
        if (speed > MAX_SPEED) {
            speed = MAX_SPEED;
        }
    }

    public Vector2 getPos() {
        return pos;
    }

    public Vector2 getVel() {
        return vel;
    }

    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        final int widthByAspectRatio = (int)(panelWidth / BrickBreaker.ASPECT_RATIO);
        g2.setColor(Color.WHITE);
        g2.fillOval((int)(pos.x * widthByAspectRatio), (int)(pos.y * panelHeight),
                (int)(DIAMETER * widthByAspectRatio), (int)(DIAMETER * panelHeight));
    }
    
}
