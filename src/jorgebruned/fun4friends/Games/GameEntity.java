/*
 * Multiplayer Framework (Fun4Friends)
 * Final Project - Advanced Programming
 * Jorge Bruned Alamán
 */
package jorgebruned.fun4friends.Games;

import java.awt.Graphics2D;
import java.io.Serializable;

/**
 * Every game entity extends from this class, it's used to store the game status.
 * 
 * Doesn't contain a "Vector2 position" since not all GameEntities have a position
 * and some of them just need the x or y position.
 * 
 * @author Jorge Bruned Alamán
 */
public abstract class GameEntity implements Serializable {
    
    public abstract void draw(Graphics2D g2, int panelWidth, int panelHeight);
    
}